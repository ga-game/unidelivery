<?php
// costanti di configurazione


define('APP_PATH', dirname(realpath('.')) . DIRECTORY_SEPARATOR); // path a cartella elaborato, usato per include

define('VIEWS_PATH', APP_PATH . 'views' . DIRECTORY_SEPARATOR); // path a cartella views, usato per include

define('TEMPLATES_PATH', 'templates' . DIRECTORY_SEPARATOR); // dentro views, usato per include

define('WWW_PATH', dirname(realpath('.')) . DIRECTORY_SEPARATOR . 'www' . DIRECTORY_SEPARATOR); // path a cartella elaborato, usato per include

define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'unidelivery');


?>
