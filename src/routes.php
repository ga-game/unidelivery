<?php

require_once('utils.php');
require_once('templating.php');
require_once('database/db.php');

function view($file_customer, $file_supplier = NULL, array $args = []) {
  return function() use($file_customer, $file_supplier, $args) {
    save_back_url();
	if (Auth::get() !== NULL && Auth::get()->isSupplier() && $file_supplier !== NULL) {
		load_view($file_supplier, $args);
	} else {
		load_view($file_customer, $args);
	}
  };
}

function controller($class, $method = NULL) {
  $class = '\\Controllers\\' . $class;
  if(is_null($method)){
    return $class;
  }
  return[
    'class' => $class,
    'method' => $method
  ];
}

return [
  '/init' => function() {
    db_init();
    redirect('/');
  },
  '/' => view('home'),
  '/login' => controller('AccessController', 'login'),
  '/logout' => controller('AccessController', 'logout'),
  '/registrazione' => controller('AccessController', 'signupCustomer'),
  '/registrazione-fornitore' => controller('AccessController', 'signupSupplier'),
  '/confermare-account' => controller('AccessController', 'confirmAccount'),
  '/conferma' => controller('AccessController', 'doConfirm'),
  '/ristoranti' => controller('SearchController'),
  '/ristoranti/{slug}' => controller('SearchController', 'restaurant'),
  '/ordina' => controller('OrderController', 'startOrder'),
  '/pagamento/{slug}' => controller('OrderController', 'payment'),
  '/notifiche' => controller('NotificationController', 'notifications'),
  '/modifica-listino' => controller('SupplierPanelController', 'editMenu'),
  '/area-personale' => controller('MyAreaController', 'user'),
  '/policies' => view('policies'),
  '/admin' => controller('AdminController', 'adminControlPanel'),


  '/validators/{validator}' => controller('HtmlValidatorController'),

  '/api/notifications' => controller('ApiController', 'notifications'),
  '/api/notification/{id}' => controller('ApiController', 'notification'),
  '/api/users' => controller('ApiController', 'users'),
  '/api/supplier-tags' => controller('ApiController', 'supplierTags'),
  '/api/form-validation/assert-email-unregistered' => controller('ApiFormValidationController', 'assertEmailUnregistered'),
  '/api/form-validation/assert-supplier-unregistered' => controller('ApiFormValidationController', 'assertSupplierUnregistered'),

  '/test' => controller('TestController'),
  '/mail' => controller('TestController', 'mail')

]; // fine
?>
