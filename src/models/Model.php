<?php
namespace Models;
require_once('../utils.php');
require_once('../database/db.php');

class Model {
  public function __construct($queryResult) {
    foreach($queryResult as $key=>$value) {
      $this->{$key} = $value;
    }
  }

  protected static function getTableName() {
    $table = get_called_class();
    $pos = strrpos($table, '\\');
    $pos = $pos === false ? -1 : $pos;
    $table = substr($table, $pos + 1);
    return $table;
  }

  public static function create(array $params = []) {
    $query = 'INSERT INTO `' . static::getTableName() . '` (' . static::queryParameters($params)
    . ') VALUES (' . comma_sep(array_fill(0, count($params), '?')) . ')';
    db_exec($query, array_values($params));
    $id = db_last_insert_id();
    return static::queryFirst('SELECT * FROM `' . static::getTableName() . '` WHERE id=?', [$id]);
  }

  protected static function query($str, array $params = []) {
    $q = db_query($str, $params);
    $retn = [];
    foreach($q as $elem) {
      $retn[] = static::loadFromQueryResult($elem);
    }
    return $retn;
  }

  protected static function queryFirst($str, array $params = []) {
    $q = static::query($str, $params);
    $retn = NULL;
    if(count($q)>0){
      $retn = $q[0];
    }
    return $retn;
  }

  protected static function loadFromQueryResult($ob) {
    $cls = get_called_class();
    return new $cls($ob);
  }

  private static function queryParameters(array $params) {
    return comma_sep(array_map(function($e){return "`$e`";}, array_keys($params)));
  }

  protected function save() {
    $keys = array_diff(array_keys($this), ['id',]);
    $vals = [];
    foreach($keys as $k=>$v){
      $keys[$k] = $v . '=?';
      $vals[$k] = $this->{$v};
    }
    $vals[] = $this->id;
    $query = 'UPDATE `' . static::getTableName() . '` SET ' . comma_sep($keys) . ' WHERE id=?';
    return db_query($query, $vals);
  }
}
