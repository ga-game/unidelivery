<?php
namespace Models;

class User extends Model {
  protected static function getTableName() {
    return 'User';
  }

  public static function findByEmail($email) {
    return self::queryFirst('SELECT * FROM User WHERE email=?', [$email]);
  }

  public static function findById($id) {
    return self::queryFirst('SELECT * FROM User WHERE id=?', [$id]);
  }

  public static function findByConfirmationCode($code) {
    return self::queryFirst('SELECT * FROM User WHERE confirmationCode=?', [$code]);
  }

  public static function findByRememberToken($token){
    return self::queryFirst('SELECT * FROM User WHERE rememberToken=?', [$token]);
  }

  public static function findByOrder($order_id) {
    return self::queryFirst('SELECT U.* FROM User U, `order` O '
      . ' WHERE O.customer=U.id AND O.id=?'
      , [$order_id]);
  }

  protected static function loadFromQueryResult($ob) {
    switch($ob->role){
      case 'supplier':
        return new Supplier($ob);
      case 'customer':
        return new Customer($ob);
      case 'admin':
        return new User($ob);
      default:
        return NULL;
    }
  }

  public function setRememberToken($token) {
    $this->rememberToken = $token;
    db_exec('UPDATE User SET rememberToken = ? WHERE id = ?', [$token, $this->id]);
  }

  public function confirmAccount() {
    $this->confirmationCode = null;
    db_exec('UPDATE User SET confirmationCode = NULL WHERE id = ?', [$this->id]);
  }

  public function getLoginAttemps() {
    $q = db_query('SELECT COUNT(*) AS attemps FROM LoginAttemps WHERE user = ? AND date >= DATE_SUB(NOW(), INTERVAL 10 MINUTE)', [$this->id]);
    if(count($q)){
      return $q[0]->attemps;
    }
    return 0;
  }

  public function addLoginAttemp() {
    db_exec('INSERT INTO LoginAttemps(user, date) VALUES(?, NOW())', [$this->id]);
  }

  public function resetLoginAttemps() {
    db_exec('DELETE FROM LoginAttemps WHERE user=?', [$this->id]);
  }

  public function isCustomer() {
    return $this->role === 'customer';
  }

  public function isSupplier() {
    return $this->role === 'supplier';
  }

  public function isAdmin() {
    return $this->role === 'admin';
  }
}
