<?php

namespace Models;


class Notification extends Model {

  private static function reduce($data) {
    return reduce($data, array([
      'nid' => 'id',
      'dateCreated' => 'dateCreated',
      'dateRead' => 'dateRead',
      'type' => 'type',
      'order' => [
        'oid' => 'id',
        'odateDelivery' => 'dateDelivery',
        'ostatus' => 'status',
        'olocation' => 'location',
        'ocomment' => 'comment',
        'customer' => [
          'customerName' => 'name',
		  'customerSlug' => 'slug'
        ],
		'supplier' => [
		  'supplierName' => 'name',
		  'supplierSlug' => 'slug',
		  'supplierLogoPath' => 'logoPath'
		],
        'products' => array([
          'pid' => 'id',
          'pname' => 'name',
          'pprice' => 'price',
          'quantity' => 'quantity',
        ]),
      ],
    ]));
  }

  public static function unread($user) {
    $query = ' SELECT N.id as nid, N.order, N.dateCreated, N.dateRead, N.type '
            .', P.id as pid, P.name as pname, P.price as pprice '
            .', OP.quantity '
            .', O.id as oid, O.dateDelivery as odateDelivery, O.status as ostatus, O.location as olocation, O.comment as ocomment '
            .', C.name as customerName, C.slug as customerSlug '
			.', S.name as supplierName, S.slug as supplierSlug, S.logoPath as supplierLogoPath '
            . ' FROM Notification N, Order_Product OP, Product P, `Order` O, User C, Menusection M, User S '
      . ' WHERE N.destination=? '
      . ' AND N.dateRead IS NULL AND N.`order` = O.id '
      . ' AND OP.`order` = N.`order` '
      . ' AND O.customer = C.id '
      . ' AND P.id = OP.product '
	  . ' AND M.id = P.section '
      . ' AND M.supplier = S.id';
    return self::reduce(db_query($query, [$user->id]));
  }

  public static function since($user, $id){
    $query = ' SELECT N.id as nid, N.order, N.dateCreated, N.dateRead, N.type '
            .', P.id as pid, P.name as pname, P.price as pprice '
            .', OP.quantity '
            .', O.id as oid, O.dateDelivery as odateDelivery, O.status as ostatus, O.location as olocation, O.comment as ocomment '
            .', C.name as customerName, C.slug as customerSlug '
			.', S.name as supplierName, S.slug as supplierSlug, S.logoPath as supplierLogoPath '
            . ' FROM Notification N, Order_Product OP, Product P, `Order` O, User C, Menusection M, User S '
      . ' WHERE N.destination=? '
      . ' AND N.id>? AND N.`order` = O.id '
      . ' AND OP.`order` = N.`order` '
      . ' AND O.customer = C.id '
      . ' AND P.id = OP.product '
	  . ' AND M.id = P.section '
      . ' AND M.supplier = S.id';
    return self::reduce(db_query($query, [$user->id, $id]));
  }
  
  public static function all($user){
    $query = ' SELECT N.id as nid, N.order, N.dateCreated, N.dateRead, N.type '
            .', P.id as pid, P.name as pname, P.price as pprice '
            .', OP.quantity '
            .', O.id as oid, O.dateDelivery as odateDelivery, O.status as ostatus, O.location as olocation, O.comment as ocomment '
            .', C.name as customerName, C.slug as customerSlug '
			.', S.name as supplierName, S.slug as supplierSlug, S.logoPath as supplierLogoPath '
            . ' FROM Notification N, Order_Product OP, Product P, `Order` O, User C, Menusection M, User S '
      . ' WHERE N.destination=? '
      . ' AND N.`order` = O.id '
      . ' AND OP.`order` = N.`order` '
      . ' AND O.customer = C.id '
      . ' AND P.id = OP.product '
	  . ' AND M.id = P.section '
      . ' AND M.supplier = S.id';
    return self::reduce(db_query($query, [$user->id]));
  }
}
