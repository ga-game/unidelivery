<?php
namespace Models;

class Order extends Model {
  public static function create(array $args = []) {
    $products = [];
    if(isset($args['products'])){
      $products = $args['products'];
      unset($args['products']);
    }
    $order = parent::create($args);
    $order->addProducts($products);
    return $order;
  }

  public function addProducts(array $prods) {
    if(count($prods) <= 0){
      return;
    }
    $query = 'INSERT INTO Order_Product(`order`, product, quantity) VALUES ';
    $query .= str_repeat('(?,?,?),', count($prods)-1) . '(?,?,?)';
    $data = [];
    foreach($prods as $id=>$qty){
      $data[] = $this->id;
      $data[] = $id;
      $data[] = $qty;
    }
    db_exec($query, $data);
  }
}
