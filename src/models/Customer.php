<?php
namespace Models;

class Customer extends User {
  public static function create(array $data = []) {
    $data['role'] = 'customer';
    parent::create($data);
  }

  protected static function loadFromQueryResult($ob) {
    return new Customer($ob);
  }
}
