<?php

namespace Models;

class Product extends Model {
  public static function findByIds(array $ids){
    $qMarks = str_repeat('?,', count($ids) - 1) . '?';
    $query = "SELECT * FROM Product WHERE ID IN ($qMarks) ORDER BY name";
    return self::query($query, $ids);
  }
}
