<?php

namespace Models;

class Supplier extends User {
  public static function create(array $data = []) {
    $data['role'] = 'supplier';
    parent::create($data);
  }

  public static function search($query) {
    return self::query(
      '(SELECT U.* FROM User U, SupplierTag T, Supplier_Tagged ST'
      . ' WHERE U.role="supplier" '
      . '  AND (U.id=ST.supplier  '
      . '  AND ST.tag=T.name  '
      . '  AND T.name LIKE ?) '
      . ' ORDER BY T.name) '
      . ' UNION '
      . '(SELECT * FROM User WHERE role="supplier" AND name LIKE ? ORDER BY name)'
      , ["%$query%", "%$query%"]);
  }

  public static function findBySlug($slug) {
    return self::queryFirst('SELECT * FROM User WHERE role="supplier" AND slug=?', [$slug]);
  }

  public static function findByProduct($id) {
    return self::queryFirst('SELECT U.* FROM User U, MenuSection MS, Product P '
      . ' WHERE U.role="supplier" AND MS.supplier=U.id AND P.section = MS.id AND P.id=?'
      , [$id]);
  }

  protected static function loadFromQueryResult($ob) {
    return new Supplier($ob);
  }

  public function getMenu() {
      $qr = db_query(
          ' SELECT MS.id as sectionId, MS.name as sectionName, P.id as productId, P.name as productName, P.price as productPrice '
        . ' FROM MenuSection MS, Product P '
        . ' WHERE MS.supplier = ? AND NOT MS.deleted AND P.section=MS.id AND NOT P.deleted '
        . ' ORDER BY MS.pos, MS.name, P.posInSection, P.name'
        , [$this->id]);

      $sections = [];
      foreach($qr as $row) {
        $sid = $row->sectionId;
        if(!isset($sections[$sid])){
          $sections[$sid] = [
            'id' => $row->sectionId,
            'name' => $row->sectionName,
            'products' => []
          ];
        }

        $sections[$sid]['products'][$row->productId] = new Product([
          'id' => $row->productId,
          'name' => $row->productName,
          'price' => $row->productPrice,
          'section' => $row->sectionId
        ]);
      }

      return array_map(function($section) {return new MenuSection($section);}, $sections);
  }

  public function deleteMenuSections() {
    db_exec('UPDATE MenuSection SET deleted=true WHERE supplier=?', [$this->id]);
  }
  
  public function deleteMenuProducts() {
    db_exec('UPDATE Product P INNER JOIN Menusection M ON P.section = M.id SET P.deleted=true WHERE M.supplier=?', [$this->id]);
  }
}
