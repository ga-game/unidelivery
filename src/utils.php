<?php
/*
 * Funzioni di utilità generiche
**/
require_once('url_slug.php');

function sec_session_start() {
        $session_name = 'sec_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function abort($code, $message = null) {
  http_response_code($code);
  die($message);
}

function redirect($destination) {
 // NOTA: non dovrebbe esserci output prima di questa istruzione!
 header('Location: ' . $destination);
 exit();
}

function redirect_back() {
  $prev = '/';
  if(!empty($_SESSION['previous-url'])){
    $prev = $_SESSION['previous-url'];
  }
  redirect($prev);
}

function save_back_url() {
  if(strtolower($_SERVER['REQUEST_METHOD']) === 'get'
      && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')){
    $_SESSION['previous-url'] = $_SERVER['REQUEST_URI'];
  }
}

function redirect_back_back() {
  $prev = '/';
  if(!empty($_SESSION['previous-url2'])){
    $prev = $_SESSION['previous-url2'];
  }
  redirect($prev);
}

function getcookie($name) {
  return isset($_COOKIE[$name]) ? $_COOKIE[$name] : null;
}

function unsetcookie($name) {
  if (isset($_COOKIE[$name])) {
    setcookie($name, '', 1, '/');
    unset($_COOKIE[$name]);
  }
}

function sep(array $elems, $sep){
  $putSep = false;
  $str = '';
  foreach($elems as $e){
    if($putSep){
      $str .= $sep;
    } else {
      $putSep = true;
    }
    $str .= $e;
  }
  return $str;
}

function comma_sep(array $elems) {
  return sep($elems, ',');
}


function request_value($key, $default = null){
  return isset($_REQUEST[$key]) && !empty($_REQUEST[$key]) ? $_REQUEST[$key] : $default;
}


function reduce($dataSet, array $groupingKeys) {
  $set = [];
  // prima chiave = id
  //var_dump($groupingKeys);die($idKey);
  $idKey = array_keys($groupingKeys)[0];
  if(is_string($idKey)){
    // raggruppamento object
    // situazione ['id' => 'xx', 'prop' => 'xy']
    if(count($dataSet)>0) {
      $elem = $dataSet[0];
      foreach($groupingKeys as $key => $group) {
        if(is_array($group)){
          $set[$key] = reduce($dataSet, $group);
        } else {
          $set[$group] = $elem->{$key};
        }
      }
    }
  } else {
    // raggruppamento array of objects
    // situazione [['id' => 'xx', 'prop' => 'xy']]
    $groupingKeys = $groupingKeys[$idKey];
    $idKey = array_keys($groupingKeys)[0];
    foreach($dataSet as $elem) {
      $id = $elem->{$idKey};
      if(!isset($set[$id])){
        $set[$id] = [];
        $groupedDataSet[$id] = [];
      }
      $groupedDataSet[$id][] = $elem;
    }
    foreach($set as $id=>$val){
      $set[$id] = reduce($groupedDataSet[$id], $groupingKeys);
    }

    $set = array_values($set);
  }
  return $set;
}

// escape
function e($var){
  return htmlspecialchars($var);
}

function random_string($len)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randstring = '';
    for ($i = 0; $i < $len; $i++) {
        $randstring .= $characters[rand(0, strlen($characters)-1)];
    }
    return $randstring;
}

 ?>
