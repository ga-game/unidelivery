<?php
/*
* Funzioni per il routing delle richieste HTTP
*
**/

require_once('config.php');
require_once('utils.php');


function get_url_path($url) {
  return parse_url($url, PHP_URL_PATH);
}

function make_route_pattern($route) {
  $route = preg_quote($route, '/'); // escape per regex
  $route = str_replace('\{', '(?P<', $route); // sostiuisce graffe con capturing group
    $route = str_replace('\}', '>.+?)', $route);
    $route = '/' . $route . '$/iA'; // completa pattern con flag insensitive, Anchored (dall'inizio), $ (fino alla fine)
    return $route;
  }

  function get_route_handler($routeData) {
    $handler = null;
    if(is_callable($routeData)){
      $handler = $routeData;
    } else if(is_array($routeData)){
      $method = strtolower($_SERVER['REQUEST_METHOD']);
      if(isset($routeData[$method])) {
        $handler = get_route_handler($routeData[$method]);
      } else if(isset($routeData['class'])) {
        $controller = new $routeData['class'];
        $method .= ucfirst($routeData['method']);
        if(method_exists($controller, $method)) {
          $handler = [$controller, $method];
        }
      }
    } else if(is_string($routeData)){
      // $handler = function() use ($routeData) { echo $routeData; };
      $controller = new $routeData();
      $method = strtolower($_SERVER['REQUEST_METHOD']);
      if(method_exists($controller, $method)) {
        $handler = [$controller, $method];
      }
    }
    return $handler;
  }

  function dispatch_request() {
    $routes = require('routes.php');
    $path = get_url_path($_SERVER['REQUEST_URI']);

    foreach ($routes as $route => $routeData) {
      $pattern = make_route_pattern($route);
      if(1 === preg_match($pattern, $path, $matches)) {
        // toglie valori con indice intero da array di matches, rimangono i capture groups della regex
        foreach($matches as $k => $v) { if(is_int($k)) { unset($matches[$k]); } }

        $handler = get_route_handler($routeData);
        if(is_null($handler)){
          abort(404);
        }
        // chiama $handler passandogli i parametri dati da $matches
        $retn = call_user_func_array($handler, $matches);

        /*
        $exclude = ['/conferma', '/confermare-account'];
        $currentUrl = $_SERVER['REQUEST_URI'];
        if(strtolower($_SERVER['REQUEST_METHOD']) === 'get' && !in_array($path, $exclude)
          && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) !== 'xmlhttprequest')){
          if(isset($_SESSION['previous-url']) && (!isset($_SESSION['previous-url2']) || $_SESSION['previous-url'] !== $currentUrl)){
            $_SESSION['previous-url2'] = $_SESSION['previous-url'];
          }
          $_SESSION['previous-url'] = $currentUrl;
        }
        var_dump($_SESSION);
        */
        return $retn;
      }
    }
    abort(404);
  }

  ?>
