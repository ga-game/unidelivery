<?php
const REQUIRED_VERSION = '7';
if (version_compare(phpversion(), REQUIRED_VERSION, '<')) {
  die('Necessario PHP versione >= ' . REQUIRED_VERSION . '. La tua versione: ' . phpversion());
}
require_once('../utils.php');

sec_session_start();
set_time_limit(30);
require_once('../config.php');
spl_autoload_register(function ($class_name) {
  $str = str_replace('\\', DIRECTORY_SEPARATOR, $class_name);
  require(APP_PATH . $str . '.php');
});

require_once('../routing.php');
dispatch_request();
?>
