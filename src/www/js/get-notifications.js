$(document).ready(function () {
  var container = $('<aside id="notifications-container"></aside>');
  var lastNotificationId = null;
  if($('footer').length) {
    $('footer').before(container);
  } else {
    $('body').append(container);
  }

  function spawnNotification(notif) {
    var data = '<div class="alert alert-dismissible fade show slideOut">';
	if (notif.type === "order-sent") {
		data += '<time class="timeago" datetime="'+new Date(notif.dateCreated).toISOString()+'"></time>';
		data += '<p><a href="/ristoranti/' + notif.order.supplier.slug + '">' + notif.order.supplier.name + '</a> ha spedito il tuo ordine #';
		data += notif.order.id + ' verso ' + notif.order.location + '</p>';
	} else if (notif.type === "order-created") {
		data += '<time class="timeago" datetime="'+new Date(notif.dateCreated).toISOString()+'"></time>';
		data += '<p>Nuovo ordine #' + notif.order.id + ' da ' + notif.order.customer.name+'</p>';
	}
	data += '<p><a href="/notifiche#ordine' + notif.order.id + '">Ulteriori dettagli</a></p>';
    data += '<button type="button" class="close" data-dismiss="alert" aria-label="Chiudi">';
    data += '<span aria-hidden="true">&times;</span>';
    data += '</button>';
    data += '</div>';
    var node = $(data);
    node.on('close.bs.alert', function () {
        $.post('/api/notification/' + notif.id, {
          dateRead: new Date().toISOString()
        }, notif.order.supplier.name)
    });
    container.append(node);
    node.find('.timeago').timeago();
  }


  function pollNotifications() {
    $.getJSON('/api/notifications',
      {
        since: lastNotificationId
      }, function( data ) {
      for(var i=0;i<data.length;i++){
        var notifData = data[i];
        if(typeof lastNotificationId === 'string'){
          lastNotificationId = notifData.id;
        } else {
          lastNotificationId = Math.max(notifData.id, lastNotificationId);
        }
        spawnNotification(notifData);
      }
    });
  }

  pollNotifications();
  setInterval(function() {
    pollNotifications();
  }, 5000);
});
