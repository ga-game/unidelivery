
$(document).ready(function () {
	$('.navbar-collapse').on('show.bs.collapse', function() {
		$('.burger-icon').addClass('open');
	});
	$('.navbar-collapse').on('hide.bs.collapse', function() {
		$('.burger-icon').removeClass('open');
	});
});
