$(document).ready(function () {
  var queued_container = $('#queued-orders ol');
  var sent_container = $('#sent-orders ol');
  var orderArrayIndex = 0;
  var lastNotificationId = "all";
	
  function spawnNotification(notif) {
    var data = '<li id="ordine' + notif.order.id + '">';
	data += '<form method="post" class="rounded">';
	data += '<div class="row">';
	data += '<div class="col-12">';
	if (notif.order.status == "queued") {
	data += '<input type="hidden" name="order[' + orderArrayIndex + '][order_id]" value="' + notif.order.id + '">';
	}
	data += '<p><time class="timeago" datetime="'+new Date(notif.dateCreated).toISOString()+'"></time></p>';
	data += '<h2>Ordine #' + notif.order.id + '</h2>';
	data += '<p>Cliente: ' + notif.order.customer.name + '</p>';
	data += '<p>Consegna in ' + notif.order.location + ' il ' + notif.order.dateDelivery.replace(/ /g," alle ") +'</p>';
	data += '<ol class="ordered-products">';
	for(var p=0;p<notif.order.products.length;p++){
      var prod = notif.order.products[p];
      data += '<li>';
      data += prod.quantity + ' x ' + prod.name + ' €' + prod.price;
      data += '</li>';
    }
	data += '</ol>';
	if (notif.order.status == "queued") {
		data += '<label for="order[' + orderArrayIndex + '][comment]" class="sr-only">Commento</label>';
		data += '<input type="text" id="order[' + orderArrayIndex + '][comment]" name="order[' + orderArrayIndex + '][comment]" class="form-control" placeholder="Commento (opzionale)">';
	} else {
		data += '<p>' + notif.order.comment + '</p>';
	}
	data += '</div>';
	data += '</div>';
	if (notif.order.status == "queued") {
		data += '<div class="accept-order">';
		data += '<button type="submit" value="Accetta ordine" class="confirm-order btn btn-link" aria-label="Accetta ordine">Accetta ordine <span class="fas fa-check"></span></button>';
		data += '</div>';
	}
	data += '</form>';
	data += '</li>';
    var node = $(data);
	if (notif.order.status == "queued") {
		queued_container.prepend(node);
		$('#queued-orders > p').text('');
	} else {
		sent_container.prepend(node);
		$('#sent-orders > p').text('');
	}
	if (notif.order.status == "queued") {
		node.find('.accept-order').on('click', function() {
			$.post('/api/notification/' + notif.order.id, {
			  comment: node.find('input[name$="[comment]"]').val()
			})
		});
	}
    node.find('.timeago').timeago();
  }


  function pollNotifications() {
    $.getJSON('/api/notifications',
		{
			since: lastNotificationId
		}, function( data ) {
			for(var i=0;i<data.length;i++) {
				var notifData = data[i];
				if (notifData.type == "order-created") {
					if (notifData.dateRead === null) {
						$.post('/api/notification/' + notifData.id, {
						  dateRead: new Date().toISOString()
						});
					}
					if(typeof lastNotificationId === 'string'){
						lastNotificationId = notifData.id;
					} else {
						lastNotificationId = Math.max(notifData.id, lastNotificationId);
					}
					spawnNotification(notifData);
					orderArrayIndex++;
				}
		  }
    });
  }
  
  pollNotifications();
  setInterval(function() {
    pollNotifications();
  }, 5000);
});
