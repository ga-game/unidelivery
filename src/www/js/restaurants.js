$(document).ready(function() {
	$(".icon").on('error', function() {
		$(this).attr('src', '/res/default-icon.jpg');
	});
	
	if ($("#search-panel ul").is(":empty")) {
		$("#search-panel ul").append("<p>Nessun ristorante trovato.</p>");
	}
});