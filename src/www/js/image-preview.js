$(document).ready(function () {

  $("#icon-wrapper").hide();
  $("#cover-wrapper").hide();
  $("#icon-preview").hide();
  $("#cover-preview").hide();

  function readURL(input, target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $(target).attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#icon").change(function() {
    readURL(this, '#icon-preview');
    $("#icon-wrapper").show();
    $("#icon-preview").show();
  });

  $("#cover").change(function() {
    readURL(this, '#cover-preview');
    $("#cover-wrapper").show();
    $("#cover-preview").show();
  });

});
