$(document).ready(function () {

	var num_sections = $('.section').length;
	var num_products = $('.product').length;

	$(document).on('click', '.new-section', function() {
		var ol_sections = $("#list-form").find("ol").get(0);

		$(ol_sections).append("<li class='section'>" +
							"<div class='container'>" +
							"<div class='row align-items-center'>" +
							"<div class='col-1'>" +
							"<button type='button' class='remove-section btn btn-link' aria-label='Rimuovi categoria'><span class='far fa-minus-square'></span></button>" +
							"</div>" +
							"<div class='col-8'>" +
							"<label for='section["+num_sections+"][name]' class='sr-only'>Modifica categoria "+(num_sections+1)+"</label>" +
							"<input type='text' id='section["+num_sections+"][name]' name='section["+num_sections+"][name]' value='Categoria "+(num_sections+1)+"' class='w-100'>" +
							"</div>" +
							"<div class='col'>" +
							"<button type='button' class='accordion-icon btn btn-link p-0 col-3' aria-label='Espandi categoria'><span class='fas fa-chevron-down'></span></button>" +
							"</div>" +
							"</div>" +
							"</div>" +
							"<div class='accordion'><ol></ol>" +
							"<button type='button' class='new-product btn btn-link' aria-label='Aggiungi un nuovo prodotto'><span class='far fa-plus-square'></span> Aggiungi un nuovo prodotto</button></div>" +
							"</li>");

	$(".section:last-of-type").hide().fadeIn("fast");

		num_sections++;
	});

	$(document).on('keyup', "input[id^=section]", function() {
		$(this).closest("li.section").find("input[id$='[section]']").val($(this).val());
		console.log($(this).closest("li.section").find("input[id$='[section]']").val());
	});

	$(document).on('click', '.new-product', function() {
		var ol_products = $(this).prev().get(0);

		var li_product = $("<li class='product'>" +
								"<div class='container'>" +
								"<div class='row align-items-center'>" +
								"<div class='col-1'>" +
								"<button type='button' class='remove-product btn btn-link' aria-label='Rimuovi prodotto'><span class='far fa-minus-square'></span></button>" +
								"</div>" +
								"<input type='hidden' id='product[" + num_products + "][section]' name='product[" + num_products + "][section]' value='" + ($(ol_products).parent().parent().find("input[id^='section']").val()) + "'></input>" +
								"<div class='col-4'>" +
								"<label for='product["+ num_products +"][name]' class='sr-only'>Modifica nome prodotto "+(num_products+1)+"</label>" +
								"<input type='text' id='product[" + num_products + "][name]' name='product[" + num_products + "][name]' value='Prodotto' class='w-100'></input>" +
								"</div>" +
								"<div class='col-4'>" +
								"<label for='product[" + num_products + "][price]' class='sr-only'>Modifica prezzo prodotto "+(num_products+1)+"</label>" +
								"<span class='input-symbol-euro'><input type='number' min='0' step='any' id='product[" + num_products + "][price]' name='product[" + num_products + "][price]' value='0.00' class='price w-100'></input></span>" +
								"</div>" +
								"</div>" +
								"</div>" +
								"</li>");
		$(ol_products).append(li_product);

		$(li_product).find("input[type='number']").get(0).onkeydown = function(e) {
				if(!((e.keyCode > 95 && e.keyCode < 106)
				  || (e.keyCode > 47 && e.keyCode < 58)
				  || e.keyCode == 8)) {
					return false;
				}
			};

		$(this).parent().find(".accordion-icon svg").removeClass("fa-chevron-down");
		$(this).parent().find(".accordion-icon svg").addClass("fa-chevron-up");
		$(ol_products).closest("div.accordion").get(0).style.maxHeight = $(ol_products).closest("div.accordion").get(0).scrollHeight + "px";

		num_products++;
	});

	$(document).on('click', '.remove-section', function() {
		$(this).closest("li.section").remove();
	});

	$(document).on('click', '.remove-product', function() {
		$(this).closest("li.section").remove();
	});

	$(document).on('click', '.accordion-icon', function() {
		$(this).find("svg").toggleClass("fa-chevron-up fa-chevron-down");
		var ol_products = $(this).closest("li.section").find("div.accordion").get(0);
		if (ol_products.style.maxHeight){
		  ol_products.style.maxHeight = null;
		} else {
		  ol_products.style.maxHeight = ol_products.scrollHeight + "px";
		}
	});

	setTimeout(function() {
		var section_ol = $("#list-form li.section:first-child").find("div.accordion").get(0);
		$("#list-form li.section:first-child").find(".accordion-icon svg").toggleClass("fa-chevron-down fa-chevron-up");

		  section_ol.style.maxHeight = section_ol.scrollHeight + "px";

	}, 100);

});
