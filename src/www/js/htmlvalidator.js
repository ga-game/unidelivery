


$(document).ready(function () {
  function getHtml() {
    // new XMLSerializer().serializeToString(document);
    return new XMLSerializer().serializeToString(document.doctype) + document.documentElement.outerHTML;
  }
  function setCookie(key, value) {
      var expires = new Date();
      expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
      document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
  }

  function getCookie(key) {
      var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
      return keyValue ? keyValue[2] : null;
  }

  if(getCookie('htmlvalidator') == 'true'){
    $('#htmlvalidator-enable').prop('checked', true);
    $('#validator-dialog').show();
  } else {
    $('#htmlvalidator-enable').prop('checked', false);
    $('#validator-dialog').hide();
  }

  $('#htmlvalidator-enable').change(function() {
        if(this.checked) {
            setCookie('htmlvalidator', 'true');
            $('#validator-dialog').show();
        } else {
            setCookie('htmlvalidator', 'false');
            $('#validator-dialog').hide();
        }
  });

    $('#validator-dialog button').click(function() {
      var button = $(this);
      button.hide();
      button.siblings('span').show();
      var checkName = button.attr('name');
      $.post('/validators/' + checkName, {html: getHtml()}, function(response){
        var w = window.open('', checkName, 'toolbar=0,location=0,menubar=0');
        w.document.open();
        w.document.write(response);
        w.document.close();
        button.show();
        button.siblings('span').hide();
        /*
        $(w.document).ready(function(){
           console.log('AChecker ready!');
        });
        */
      });
    });
});
