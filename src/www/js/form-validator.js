$(document).ready(function(){
  $.fn.extend({
    checkValidity: function() {
      var valid = true;
      this.each(function(){
        if(!this.checkValidity()){
          valid = false;
        }
      });
      return valid;
    },
    setCustomValidity: function(message) {
      this.each(function(){
        this.setCustomValidity(message);
      });
    }
  });

  var valuesCache = {};

  //http://davidwalsh.name/javascript-debounce-function
  function debounce(func, wait, immediate) {
      var timeout;
      return function() {
          var context = this, args = arguments;
          var later = function() {
              timeout = null;
              if (!immediate) func.apply(context, args);
          };
          var callNow = immediate && !timeout;
          clearTimeout(timeout);
          timeout = setTimeout(later, wait);
          if (callNow) func.apply(context, args);
      };
  };

  function checkFormSubmitButton(form){
    return; // disabilitato
    var disabled = form.find('input:invalid').length > 0
      || form.find('input.is-invalid').length > 0
      || form.find('input[data-validate]:not(.is-valid)').length > 0;
    form.find('button[type="submit"]').prop('disabled', disabled);
  }

  function setValid(element, valid, message){
    if(valid === true){
      element.removeClass('is-invalid');
      element.addClass('is-valid');
      element.setCustomValidity("");
    } else {
      element.addClass('is-invalid');
      element.removeClass('is-valid');
      element.setCustomValidity(message);
    }
    updateFeedback(element, valid, message);
  }

  function updateFeedback(element, valid, message) {
    var feedbackNode = element.siblings('.invalid-feedback.byvalidator').first();
    if(feedbackNode.length <= 0){
      feedbackNode = $('<div class="invalid-feedback byvalidator"></div>');
      feedbackNode.insertAfter(element.parent().children(":last"));
    }
    if(valid || message.length <= 0){
      feedbackNode.hide();
    } else {
      feedbackNode.html(message);
      feedbackNode.show();
    }
    return feedbackNode;
  }

  var VALIDATORS = {
    server: function(element, url) {
      var elName = element.attr('name');
      var elVal = element.val();
      var requestData = {};
      requestData[elName] = elVal;
      var loadingNode = element.closest('.loading');
      if(loadingNode.length <= 0){
        loadingNode = $('<div class="loading"><span class="fas fa-spinner fa-spin"></span></div>');
        loadingNode.insertAfter(element);
        loadingNode.show();
        var pos = element.offset();
        loadingNode.offset({
          left: pos.left + element.outerWidth(true),
          top: pos.top
        });
      }
      loadingNode.show();
      //element.removeClass('is-valid');
      $.post(url, requestData, function(data) {
        if(element.val() === elVal){
          var valid = data && data.valid;
          var msg = '';
          if(!valid){
            msg = data.message;
          }
          setValid(element, valid, msg);
          valuesCache[elName] = {
            val: elVal,
            result: [valid, msg]
          };
        }
        loadingNode.hide();
        checkFormSubmitButton(element.closest('form'));
      });
      return [false, ''];
    },
    sameAs: function(element, otherElemName) {
      var valid = element.val() === element.closest('form').find('input[name="'+otherElemName+'"]').val();
      return [valid, 'Il valore inserito non corrisponde'];
    },
    email: function(element) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return [re.test(String(element.val()).toLowerCase()), 'Email mal formattata'];
    },
    pattern: function(element, pattern) {
      return [new RegExp(pattern).test(element.val()), ''];
    },
    afterNow: function(element) {
      var now = new Date();
      var elDate = {};
      if(element.attr('type') === 'date'){
        elDate = new Date(element.val());
        elDate.setHours(23, 59, 59);
      } else if(element.attr('type') === 'time') {
        elDate = new Date();
        var fields = element.val().split(':');
        elDate.setHours(fields[0], fields[1], 59);
      }
      return [elDate >= now, 'Non può essere nel passato'];
    },
    afterNowWithDate: function(element, selector){
      var now = new Date();
      var elDate = new Date($(selector).val());
      if(element.attr('type') === 'time') {
        var fields = element.val().split(':');
        elDate.setHours(fields[0], fields[1], 59);
      }
      return [elDate >= now, 'Non può essere nel passato'];
    }
  };

  function parseValidators(element, dataValidate){
    // split parametri. ad esempio il validatore è come segue:  nomeValidatore(parametro1, 2, 3)
    var validators = [];
    var validats = dataValidate.split(';')
    for(var i=0;i<validats.length;i++){
      validator = validats[i].trim();
      var params = [element,];
      var pos = validator.indexOf('(');
      if(pos > 0){
        params = params.concat(
          validator.slice(pos+1, validator.lastIndexOf(')'))
          .split(',')
          .map(function(e){return e.trim();})
        );
        validator = validator.slice(0, pos);
      }
      validators.push({name:validator, args: params});
    }
    return validators;
  }

  function validate(element) {
    var dataValidate = element.data('validate');
    var elName = element.attr('name');
    var elVal = element.val();
    if(elName in valuesCache && valuesCache[elName].val === elVal){
      setValid(element, valuesCache[elName].result[0], valuesCache[elName].result[1]);
      return valuesCache[elName].result[0];
    }
    var validated = [true, ''];

    if(validated[0] && element.prop('required') && element.val().length <= 0){
      element.addClass('is-invalid');
      validated = [false, 'Campo richiesto'];
    }
    if(validated[0] && element.prop('type') === 'email'){
      validated = VALIDATORS.email(element);
    }
    if(validated[0] && element.prop('pattern')){
      validated = VALIDATORS.pattern(element, element.prop('pattern'));
    }

    var shouldCache = true;
    if(validated[0] && dataValidate) {
      element.setCustomValidity('Controllo validità in corso...');
      // split parametri. ad esempio il validatore è come segue:  nomeValidatore(parametro1, 2, 3)
      var validators = parseValidators(element, dataValidate);
      for(var i=0;i<validators.length && validated[0];i++){
        // chiama il validatore(param1, 2, 3)
        validated = VALIDATORS[validators[i].name].apply(null, validators[i].args);
        if(validators[i].name === 'afterNowWithDate'){
          shouldCache = false;
        }
      }
    }
    setValid(element, validated[0], validated[1]);
    if(shouldCache){
      valuesCache[elName] = {
        val: element.val(),
        result: validated
      };
    }
    checkFormSubmitButton(element.closest('form'));
    return validated[0];
  }

  $('input').each(function(){
    var element = $(this);

    var dataValidate = element.data('validate');
    if(dataValidate) {
      var validators = parseValidators(element, dataValidate);
      //var linkedValidations = [];
      for(var i=0;i<validators.length;i++){
        if(validators[i].name === 'sameAs'){
          var otherElemName = validators[i].args[1];
          var otherElem = element.closest('form').find('input[name="'+otherElemName+'"]');
          //linkedValidations.push(otherElem);
          otherElem.keyup(debounce(function(event){
            validate(element);
          }, 250));
        }
          if(validators[i].name === 'afterNowWithDate'){
            var otherElem = $(validators[i].args[1]);
            //linkedValidations.push(otherElem);
            otherElem.keyup(debounce(function(event){
              validate(element);
            }, 250));
          }
      }
    }
    element.keyup(debounce(function(event){
      validate(element);
    }, 250));
    /*
    element.change(function(event) {
      validate(element);
    });
    */
  });

  $('form').each(function(){
    $(this).attr('novalidate', '');
      $(this).submit(function(e){
        var valid = true;
        $(this).find('[type="submit"]').prop('disabled', true);
        $(this).find('input').each(function(){
          valid = valid && validate($(this));
        });
        $(this).addClass('was-validated');
        valid = valid && this.checkValidity();
        if(valid){
          var pwd = $(this).find('input[name="password"]');
          var hash = hex_sha512(pwd.val()).toUpperCase();
          pwd.val(hash); // sostituisce password in chiaro con password criptata
        }
        $(this).find('[type="submit"]').prop('disabled', false);
        return valid;
      });
      checkFormSubmitButton(this);
  });
});
