$(document).ready( function () {

	var userColumnDefs =
	  [
		 { title: "" },
		 { title: "Id" },
		 { title: "Email" },
		 { title: "Password" },
		 { title: "Nome" },
		 { title: "Nome corto" },
		 { title: "Telefono" },
		 { title: "Indirizzo" },
		 { title: "Ruolo" },
		 { title: "Codice di conferma" },
		 { title: "Percorso del logo" },
		 { title: "Percorso dell'immagine" }
	  ];

	var userDataSet = [];
	var jsonUser = (function () {
		var jsonUser = null;
		$.ajax({
			'async': false,
			'global': false,
			'url': '/api/users',
			'dataType': "json",
			'success': function (data) {
				jsonUser = data;
			}
		});
		return jsonUser;
	})();

	for(var i=0;i<jsonUser.length;i++){
        var userData = jsonUser[i];
			userDataSet.push(["", userData.id, userData.email, userData.password, userData.name, userData.shortName, userData.phone, userData.address, userData.role, userData.confirmationCode, userData.logoPath, userData.imgPath]);
      }

	var supplierTagColumnDefs =
	  [
		 { title: "Nome" },
		 { title: "" }
	  ];

	var supplierTagDataSet = [];
	var jsonSupplierTag = (function () {
		var jsonSupplierTag = null;
		$.ajax({
			'async': false,
			'global': false,
			'url': '/api/supplier-tags',
			'dataType': "json",
			'success': function (data) {
				jsonSupplierTag = data;
			}
		});
		return jsonSupplierTag;
	})();

	for(var i=0;i<jsonSupplierTag.length;i++){
        var supplierTagData = jsonSupplierTag[i];
        supplierTagDataSet.push([supplierTagData.name, "<form method='post'>"+
												"<input type='hidden' name='remove-section' value='"+supplierTagData.name+"'>"+
												"<label for='submit"+i+"' class='sr-only'>Rimuovi categoria</label>"+
												"<input id='submit"+i+"' type='submit' class='btn btn-danger' value='X'>"+
												"</form>"]);
      }

    $('#users-table').DataTable({
		"sPaginationType": "full_numbers",
		data: userDataSet,
		columns: userColumnDefs,
		dom: 'Bfrtip',
		pageLength: 20,
		select: 'single',
		responsive: true,
        language: {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Italian.json"
        }
    });

	$('#supplier-tags-table').DataTable({
		"sPaginationType": "full_numbers",
		data: supplierTagDataSet,
		columns: supplierTagColumnDefs,
		dom: 'Bfrtip',
		select: 'single',
		responsive: true,
        language: {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Italian.json"
        }
    });

	$(document).on('click', "input[value='Aggiungi categoria']", function(){
		if ($('#supplier-tags tbody tr:last').hasClass("odd")) {
			$('#supplier-tags tbody').append("<tr role='row' class='even'>"+
												"<td tabindex='0' class='sorting_1'>"+
												"<form method='post'>"+
												"<input type='hidden' name='remove-section' value='"+$(this).val()+"'>"+
												"<input type='submit' class='btn btn-link' value='Rimuovi categoria'>"+
												"</form>"+
												"</td>"+
												"<td>"+$(this).val()+"</td>"+
												"</tr>");
		} else {
			$('#supplier-tags tbody').append("<tr role='row' class='odd'>"+
												"<td tabindex='0' class='sorting_1'>"+
												"<form method='post'>"+
												"<input type='hidden' name='remove-section' value='"+$(this).val()+"'>"+
												"<input type='submit' class='btn btn-link' value='Rimuovi categoria'>"+
												"</form>"+
												"</td>"+
												"<td>"+$(this).val()+"</td>"+
												"</tr>");
		}
	});

	$(document).on('click', "button[value='Rimuovi categoria']", function() {
		$(this).closest("tr").remove();
	});

	setTimeout(function() {

		for(var i=0;i<jsonUser.length;i++){
        var userData = jsonUser[i];
		if (userData.role == "admin") {
			$("#users-table tr:nth-child("+(i+1)+")").append("<td></td>");
		} else if (userData.banned == 0) {
			$("#users-table tr:nth-child("+(i+1)+")").append("<td><form method='post'>"+
						"<input type='hidden' name='id-ban' value='"+$("#users-table tr:nth-child("+(i+1)+") td:nth-child(2)").html()+"'>" +
						"<input type='submit' class='btn btn-danger' value='Banna'>" +
						"</form></td>");


      } else {
		  $("#users-table tr:nth-child("+(i+1)+")").append("<td><form method='post'>"+
						"<input type='hidden' name='id-unban' value='"+$("#users-table tr:nth-child("+(i+1)+") td:nth-child(2)").html()+"'>" +
						"<input type='submit' class='btn btn-success' value='Sbanna'>" +
						"</form></td>");
	  }
	}

	$("table").css("width", "100%");

	$(".dataTables_info").remove();

	$("table").removeAttr("aria-describedby");

	}, 500);

} );
