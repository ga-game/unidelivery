$(document).ready(function () {

	var subtotal = 0;

	var cart = $('#cart');
	var cartForm = cart.find('.js-add-cart-form-here');
	var cartProducts = {};
	var product_id = $(this).parent().find(".product-id").text();
	var product_name = $(this).parent().find(".product-name").text();
	var product_price = $(this).parent().find(".product-price").text();

	$("#cart li.product").each(function() {
		var product_id = $(this).find(".product-id").text();
		console.log($(this).find(".product-id").text());
		var product_name = $(this).find(".product-name").text();
		var product_price = $(this).find(".product-price").text();
		var product_quantity = $(this).find(".product-quantity").text();

		cartProducts[product_id] = {id:product_id, name: product_name, price: parseFloat(product_price), quantity:product_quantity};
		subtotal = subtotal + product_price * product_quantity;
		console.log(cartProducts[product_id]);
	});


	function update_cart_form() {
		cartForm.empty();
		for(var pid in cartProducts){
			cartForm.append('<input type="hidden" name="product['+pid+']" value="' + cartProducts[pid].quantity + '" />');
		}
	}

	function update_order_button() {
		var submitButton = cart.find('form button[type="submit"]');
		if(Object.keys(cartProducts).length > 0){
			submitButton.prop('disabled', false);
		} else {
			submitButton.prop('disabled', true);
		}
	}

	update_cart_form();
	update_order_button();

	$(document).on('click', '.section-name', function() {
		$(this).find("svg").toggleClass("fa-chevron-down fa-chevron-up");
		var section_ol = $(this).parent().find("ol").get(0);
		if (section_ol.style.maxHeight){
		  section_ol.style.maxHeight = null;
		} else {
		  section_ol.style.maxHeight = section_ol.scrollHeight + "px";
		}
	});

	$(document).on('click', '.add-product', function() {
		var product_id = $(this).parent().find(".product-id").text();
		var product_name = $(this).parent().find(".product-name").text();
		var product_price = $(this).parent().find(".product-price").text();

		if(product_id in cartProducts){
			cartProducts[product_id].quantity++;
			cart.find('#product-id-' + product_id + ' .product-quantity').text(cartProducts[product_id].quantity);
			cart.find('#product-id-' + product_id + ' .product-price').text(cartProducts[product_id].quantity * cartProducts[product_id].price);
		} else {
			cartProducts[product_id] = {id:product_id, name: product_name, price: parseFloat(product_price), quantity:1};
			cart.find('ol').append("<li class='product'>" +
											"<div class='container'>" +
											"<div id='product-id-" + product_id + "' class='row align-items-center'>" +
											"<button class='remove-product btn btn-link col-2' aria-label='Rimuovi prodotto'><span class='far fa-minus-square'></span></button>" +
											"<span class='product-id'>" + product_id + "</span>" +
											"<span class='product-quantity col-2'>" + cartProducts[product_id].quantity + "</span> × " +
											"<span class='product-name col'>" + product_name +"</span>" +
											"<span class='product-price col'>" + product_price + "</span>" +
										"</div>" +
										"</div>" +
										"</li>");
		}
		subtotal = subtotal + parseFloat(product_price);
		$("#subtotal").text(subtotal.toFixed(2));
		$("#total").text((subtotal + parseFloat($("#delivery-cost").html())).toFixed(2));
		update_cart_form();
		update_order_button();

		$("#toast-container").append("<div class='alert alert-dismissible fade show slideOut'>"+
									"<p>"+product_name+" è stato aggiunto al carrello.</p>"+
									"</div>");
		$("#toast-container").find("div").delay(1800).fadeOut();
	});

	cart.on('click', '.remove-product', function() {
		var product_row = $(this).closest('.product');
		var product_price = product_row.find(".product-price").text();
		var product_id = product_row.find(".product-id").text();
		subtotal = subtotal - cartProducts[product_id].price;
		$("#subtotal").text(subtotal.toFixed(2));
		$("#total").text((subtotal + parseFloat($("#delivery-cost").html())).toFixed(2));
		cartProducts[product_id].quantity--;
		if(cartProducts[product_id].quantity<=0){
			delete cartProducts[product_id];
			product_row.remove();
		} else {
			product_row.find(".product-quantity").text(cartProducts[product_id].quantity);
			product_row.find(".product-price").text(cartProducts[product_id].quantity * cartProducts[product_id].price);
		}
		update_cart_form();
		update_order_button();
	});

	setTimeout(function() {

		if ($("#menu li.section").length > 0) {
			var section_ol = $("#menu li.section:first-child").find("ol").get(0);
			$("#menu li.section:first-child ol").parent().find(".section-name:first-child svg").toggleClass("fa-chevron-down fa-chevron-up");

			  section_ol.style.maxHeight = section_ol.scrollHeight + "px";
		}
	}, 100);

});
