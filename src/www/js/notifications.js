$(document).ready(function () {
  var list_ol = $('.list-group > ol');
  var lastNotificationId = "all";

  function spawnNotification(notif) {

		  var data = '<li id="ordine' + notif.order.id + '" class="list-group-item list-group-item-action flex-column align-items-start">';
		  data += '<div class="row">';
		  data += '<div class="col-3 col-sm-2 col-md-1">';
		  data += '<img src="' + notif.order.supplier.logoPath + '" alt="Logo ristorante" height="50" width="50">';
		  data += '</div>';
		  data += '<div class="col-9 col-sm-10 col-md-11">';
		  data += '<p class="notification-description"><a href="/ristoranti/' + notif.order.supplier.slug + '">' + notif.order.supplier.name + '</a> ha spedito il tuo ordine verso ' + notif.order.location;
		  if (notif.order.comment.length > 0) {
			  data += ': "' + notif.order.comment + '"';
		  }
		  data += '</p>';
		  data += '<ol>';
		  for(var p=0;p<notif.order.products.length;p++){
			  var prod = notif.order.products[p];
			  data += '<li>';
			  data += prod.quantity + ' x ' + prod.name;
			  data += '</li>';
			}
		  data += '</ol>';
		  data += '</div>';
		  data += '</div>';
		  data += '<p class="notification-time"><time class="timeago" datetime="'+new Date(notif.dateCreated).toISOString()+'"></time></p>';
		  data += '</li>';


    var node = $(data);
	list_ol.prepend(node);
    node.find('.timeago').timeago();
  }


  function pollNotifications() {
    $.getJSON('/api/notifications',
      {
        since: lastNotificationId
      }, function( data ) {
      for(var i=0;i<data.length;i++) {
        var notifData = data[i];
		if (notifData.type == "order-sent" && notifData.order.status == "sent") {
			if (notifData.dateRead === null) {
				$.post('/api/notification/' + notifData.id, {
				  dateRead: new Date().toISOString()
				});
			}
			if(typeof lastNotificationId === 'string'){
			  lastNotificationId = notifData.id;
			} else {
			  lastNotificationId = Math.max(notifData.id, lastNotificationId);
			}
			spawnNotification(notifData);
			$('.list-group > p').text('');
		}
      }
    });
  }

  pollNotifications();
  setInterval(function() {
    pollNotifications();
  }, 5000);

});
