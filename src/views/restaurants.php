<?=
load_template('head', [
  'title' => 'Ristoranti',
  'css' => [
    'restaurants.css'
  ],
  'js' => [
    'restaurants.js'
  ]
])
?>

  <?=load_template('header')?>

<?=load_template('order-start')?>

		<div class="container">
			<h1 class="sr-only">Ristoranti disponibili</h1>
			<div class="row">
				<section id="search-panel" class="col-12">
					<form method="get" action="/ristoranti">
						<div id="custom-search-input">
							<div class="input-group-search">
								<label for="search" class="sr-only">Cerca</label>
								<input id="search" name="cerca" class="form-control btn-lg" type="text" placeholder="Cerca" value="<?=$query?>">
								<span class="input-group-btn-search">
									<input type="submit" class="btn-search" value="submit">
								</span>
							</div>
						</div>
					</form>
					<ul><?php foreach($restaurants as $rest): ?>
						<li class="bg-light">
							<img class="icon" src="<?=$rest->logoPath?>" alt="Logo ristorante">
							<div>
								<a href="/ristoranti/<?=$rest->slug?>"><h2 class="pl-2"><?=e($rest->name)?></h2></a>
								<p class="section"><?php foreach(db_query('SELECT tag FROM supplier_tagged WHERE supplier=?', [$rest->id]) as $tag) echo $tag->tag . " " ?></p>
								<p class="address"><?= $rest->address?> - <?= $rest->phone?></p>
							</div>
						</li><?php endforeach; ?></ul>
				</section>
			</div>
		</div>
  <?=load_template('grid-end')?>

  <?=load_template('footer')?>
<?=load_template('end')?>
