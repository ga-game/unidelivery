<?=
load_template('head', [
  'title' => 'Admin',
  'css' => [
    'admin-control-panel.css'
  ],
  'js' => [
    'admin-control-panel.js'
  ]
])
?>

<?=load_template('header')?>

	<?=load_template('order-start')?>
				<h1>Pannello di controllo</h1>
					<ul class="nav nav-tabs" id="controls" role="tablist">
					  <li class="nav-item">
						<a class="nav-link active" id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="true">Utenti</a>
					  </li>
					  <li class="nav-item">
						<a class="nav-link" id="supplier-tags-tab" data-toggle="tab" href="#supplier-tags" role="tab" aria-controls="supplier-tags" aria-selected="false">Categorie</a>
					  </li>
					   <li class="nav-item">
						<a class="nav-link" id="extra-tab" data-toggle="tab" href="#extra" role="tab" aria-controls="extra" aria-selected="false">Extra</a>
					  </li>
					</ul>
				<div class="tab-content" id="controls-content">
					<div class="tab-pane fade show active" id="users" role="tabpanel" aria-labelledby="users-tab">
						<table id="users-table" class="display responsive nowrap">
						</table>
					</div>
					<div class="tab-pane fade" id="supplier-tags" role="tabpanel" aria-labelledby="supplier-tags-tab">
						<form method="post" class='new-tag-area form-inline'>
						  <div class="input-group col-12">
							<label for="add-tag" class="sr-only">Inserisci nome</label>
							<input type='text' id='add-tag' name='add-tag' placeholder="Nome">
							<input type='submit' class='btn btn-primary' value='Aggiungi'>
						  </div>
						</form>
						<table id="supplier-tags-table" class="display responsive nowrap">
						</table>
					</div>
					<div class="tab-pane fade show" id="extra" role="tabpanel" aria-labelledby="extra-tab">
						<form>
							<div class="container">
								<div class="row">
									<div class="col-12">
										<input type="checkbox" id="htmlvalidator-enable" name="subscribe" value="achecker">
										<label for="htmlvalidator-enable">Attiva validatori html</label>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
					<?=load_template('grid-end')?>



<?=load_template('end')?>
