<?=
load_template('head', [
  'title' => 'Modifica listino',
  'css' => [
    'edit-list.css'
  ],
  'js' => [
    'edit-list.js'
  ]
]);
$num_sections = 0;
$num_products = 0;
?>


	<?=load_template('header')?>

		<?=load_template('order-start')?>
			<h1>Modifica listino</h1>
			<form method="post" id="list-form" class="rounded">
			<ol>
				<?php $supplier = \Auth::require(); ?>
				<?php foreach($supplier->getMenu() as $section): ?>
					<li class="section">
						<div class="container">
							<div class="row align-items-center">
								<div class="col-1">
										<button type="button" class="remove-section btn btn-link" aria-label="Rimuovi categoria"><span class="far fa-minus-square"></span></button>
									</div>
								<div class="col-8">
									<label for="section[<?php echo $num_sections;?>][name]" class="sr-only">Modifica categoria <?php echo ($num_sections+1);?></label>
									<input type="text" id="section[<?php echo $num_sections;?>][name]" name="section[<?php echo $num_sections;?>][name]" value="<?php echo $section->name;?>" class="w-100">
								</div>
								<div class="col">
									<button type="button" class="accordion-icon btn btn-link p-0 col-3" aria-label="Espandi categoria"><span class="fas fa-chevron-down"></span></button>
								</div>
							</div>
						</div>
						<div class="accordion">
						<ol>
							<?php foreach($section->products as $product): ?>
								<li class="product">
									<div class="container">
										<div class="row align-items-center">
											<div class="col-1">
												<button type="button" class="remove-product btn btn-link" aria-label="Rimuovi prodotto"><span class="far fa-minus-square"></span></button>
											</div>
											<input type="hidden" id="product[<?php echo $num_products;?>][section]" name="product[<?php echo $num_products;?>][section]" value="<?php echo $section->name;?>"></input>
											<div class="col-4">
												<label for="product[<?php echo $num_products;?>][name]" class="sr-only">Modifica nome prodotto  <?php echo ($num_products+1);?></label>
												<input type="text" id="product[<?php echo $num_products;?>][name]" name="product[<?php echo $num_products;?>][name]" value="<?php echo $product->name;?>" class="w-100">
											</div>
											<div class="col-4">
												<label for="product[<?php echo $num_products;?>][price]" class="sr-only">Modifica prezzo prodotto <?php echo ($num_products+1);?></label>
												<span class="input-symbol-euro"><input type="number" min="0" step="any" id="product[<?php echo $num_products;?>][price]" name="product[<?php echo $num_products;?>][price]" value="<?php echo $product->price;?>" class="price w-100"></span>
											</div>
										</div>
									</div>
								</li>
								<?php $num_products++;?>
							<?php endforeach;?>
						</ol>
						<button type="button" class="new-product btn btn-link" aria-label="Aggiungi un nuovo prodotto"><span class="far fa-plus-square"></span> Aggiungi un nuovo prodotto</button>
						</div>
					</li>
					<?php $num_sections++;?>
				<?php endforeach;?>
			</ol>
			<button type="button" class="new-section btn btn-link"><span class='far fa-plus-square'></span> Aggiungi una nuova categoria</button>
		</form>
		<button type="submit" form="list-form" value="Submit" class="btn btn-block btn-primary btn-lg">Conferma</button>
		<?=load_template('grid-end')?>
    <?=load_template('footer')?>


<?=load_template('end')?>
