<?=
load_template('head', [
  'title' => 'Notifiche',
  'css' => [
  'notifications.css'
  ],
  'js' => [
  'notifications.js'
  ]
])
?>

	<?=load_template('header')?>

	<?=load_template('order-start')?>
				<h1>Notifiche</h1>
				<div class="list-group">
					<ol></ol>
					<p>Non ci sono notifiche.</p>
				</div>
			<?=load_template('grid-end')?>
    <?=load_template('footer')?>

<?=load_template('end')?>
