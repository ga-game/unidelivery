<?=
load_template('head', [
  'title' => 'Notifiche',
  'css' => [
    'order-list.css'
  ],
  'js' => [
	'order-list.js'
  ]
])
?>

<?=load_template('header')?>

		<?=load_template('order-start')?>
			<h1>Notifiche</h1>
			<ul class="nav nav-tabs" id="orders" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="queued-orders-tab" data-toggle="tab" href="#queued-orders" role="tab" aria-controls="queued-orders" aria-selected="true">Ordini in sospeso</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="sent-orders-tab" data-toggle="tab" href="#sent-orders" role="tab" aria-controls="sent-orders" aria-selected="false">Ordini spediti</a>
				</li>
			</ul>
			<div class="tab-content" id="orders-content">
				<div class="tab-pane fade show active" id="queued-orders" role="tabpanel" aria-labelledby="queued-orders-tab">
					<ol class="container"></ol>
					<p>Non ci sono ordini in sospeso.</p>
				</div>
				<div class="tab-pane fade" id="sent-orders" role="tabpanel" aria-labelledby="sent-orders-tab">
					<ol class="container"></ol>
					<p>Non ci sono ordini spediti.</p>
				</div>
			</div>
		<?=load_template('grid-end')?>

  <?=load_template('footer')?>
<?=load_template('end')?>
