<?=
load_template('head', [
  'title' => 'Conferma account',
  'css' => [
    'auth.css'
  ]
])
?>

<?=load_template('header')?>
<<?=load_template('auth-start')?>
   <h1>Ci siamo quasi!</h1>
   <p>Accedi alla tua email e conferma il tuo account</p>
<?=load_template('grid-end')?>

<?=load_template('end')?>
