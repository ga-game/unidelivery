<?=
load_template('head', [
  'title' => 'Registrati',
  'css' => [
    'auth.css'
  ]
])
?>

	<?=load_template('header')?>
    <?=load_template('auth-start')?>
       <h1>Registrazione fornitore</h1>
       <p>Inserisci i tuoi dati</p>
      <form method="post">
        <div class="form-group">
          <label for="name" class="sr-only">Nome attività</label>
        <input type="text" name="name" id="name" class="form-control" placeholder="Nome attività" required autofocus data-validate="server(/api/form-validation/assert-supplier-unregistered)">
      </div>
        <div class="form-group">
          <label for="piva" class="sr-only">Partita IVA</label>
        <input type="text" name="piva" id="piva" class="form-control" placeholder="Partita IVA" required>
      </div>
        <div class="form-group">
          <label for="email" class="sr-only">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Indirizzo e-mail" required autocomplete="email" data-validate="server(/api/form-validation/assert-email-unregistered)">
      </div>
        <div class="form-group">
          <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required autocomplete="new-password">
      </div>
        <div class="form-group">
          <label for="confirmpw" class="sr-only">Ripeti Password</label>
        <input type="password" name="confirmpw" id="confirmpw" class="form-control" placeholder="Conferma Password" required autocomplete="off" data-validate="sameAs(password)">
      </div>
      <div class="form-group form-check">
			<input type="checkbox" id="policies" value="policies" class="form-check-input" required>
			<label for="policies" class="d-inline">Accetto i <a href="policies#terms">termini e le condizioni d'uso</a>. Ho letto l'<a href="policies#privacy">informativa sulla privacy</a> e l'<a href="policies#cookies">informativa sui cookie</a>.</label>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Iscriviti</button>
        <a class="btn btn-warning btn-lg btn-block" href="/login">Hai già un account? Accedi</a>
        <a class="btn btn-warning btn-lg btn-block" href="/registrazione">Sei un cliente? Registrati qui</a>
      </form>
<?=load_template('grid-end')?>

<?=load_template('end')?>
