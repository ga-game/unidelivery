<?=
load_template('head', [
  'title' => 'UniDelivery',
  'css' => [
    'home.css'
  ]
])
?>

  <?=load_template('header')?>
	  <section class="container-fluid h-100">
		<div class="row align-items-center h-100">
		  <div class="col-12 mx-auto">
			<h1 class="text-white text-center mx-4 p-2">Ordina il tuo cibo preferito direttamente in aula!</h1>
      <p class="text-white text-center mx-4 ">Inserisci il piatto, il nome di un ristorante o il tipo di cucina a cui stai pensando</p>
			<form method="get" action="/ristoranti">
				<div id="custom-search-input">
					<div class="input-group-search">
						<label for="search" class="sr-only">Cerca</label>
						<input id="search" type="text" name="cerca" class="search-query" placeholder="Cosa cerchi?" value="">
						<span class="input-group-btn-search">
							<input type="submit" class="btn-search" value="submit">
						</span>
					</div>
				</div>
			</form>
			<h2 class="text-white text-center mx-4 mt-4 p-2">Mangiare all'università non è mai stato così comodo!</h2>
		  </div>
		</div>
	  </section>

	<?=load_template('footer')?>


<?=load_template('end')?>
