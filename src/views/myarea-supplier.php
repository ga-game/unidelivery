<?=
load_template('head', [
  'title' => 'Area personale',
  'css' => [
    'myarea.css'
  ],
  'js' => [
  'image-preview.js'
  ]
])
?>

  <?=load_template('header')?>
<?=load_template('order-start')?>

    <h1>Area Personale</h1>
    <p>Qui puoi modificare i tuoi dati</p>
    <form method="post" enctype="multipart/form-data">
        <div class="form-group">
					<label for="name" class="field">Nome</label><label class="req">*</label>
          <input name="name" value="<?=$user->name?>" type="text" id="name" class="form-control" placeholder="Nome" required>
        </div>
        <div class="form-group">
					<label for="email" class="field">E-mail</label><label class="req">*</label>
          <input name="email" value="<?=$user->email?>" type="email" id="email" class="form-control" placeholder="Indirizzo e-mail" required>
        </div>
        <label class="field">Categorie</label>
        <div class="form-check" id="categories">
          <?php foreach(db_query('SELECT * FROM suppliertag', []) as $tag): ?>
            <?=$attr ='';if(!empty(db_query('SELECT * FROM supplier_tagged WHERE supplier=? AND tag=?', [$user->id, $tag->name]))){$attr = 'checked';}?>
            <div class="checkbox ml-2">
            <label class="checkbox-inline no_indent" for="<?=$tag->name?>">
    				<input class="form-check-input" type="checkbox" id="<?=$tag->name?>" name="<?=$tag->name?>" <?=$attr?>>
            <?=$tag->name?>
          </label>
        </div>
    			<?php endforeach; ?>
        </div>
        <div class="form-group">
					<label for="address" class="field">Indirizzo</label>
          <input name="address" value="<?=$user->address?>" type="text" id="address" class="form-control" placeholder="Indirizzo">
        </div>
        <div class="form-group">
          <label for="phone" class="field">Telefono</label>
          <input name="phone" value="<?=$user->phone?>" type="number" id="phone" class="form-control" placeholder="Telefono">
        </div>
        <div class="form-group">
          <label for="icon">Icona</label>
          <input class="form-control-file mb-1" id="icon" name="icon" type="file">
          <div id="icon-wrapper" class="mb-1">
            <img id="icon-preview" src="#" alt="Anteprima Icona" />
          </div>
        </div>
        <div class="form-group">
          <label for="cover">Copertina</label>
          <input class="form-control-file mb-1" id="cover" name="cover" type="file">
          <div id="cover-wrapper" class="mb-1">
            <img id="cover-preview" src="#" alt="Anteprima Copertina" />
          </div>
        </div>
        <label class="req">*Campi obbligatori</label>
  <button class="btn btn-primary btn-lg mb-5" type="submit">Salva Modifiche</button>
</form>
<?=load_template('grid-end')?>
<?=load_template('footer')?>
<?=load_template('end')?>
