<?=
load_template('head', [
  'title' => 'Login',
  'css' => [
    'auth.css'
  ]
])
?>

<?=load_template('header')?>
<?=load_template('auth-start')?>


  <h1>Accedi a UniDelivery</h1>
  <form method="post">
  <?php if(isset($_GET['error'])): ?>
  <div class="alert alert-danger" role="alert">
    <?php if($_GET['error'] == 'pwd'): ?>
          <p>Utente inesistente o password errata</p>
    <?php endif;?>
      <?php if($_GET['error'] == 'attemps'): ?>
          <p>Rilevati troppi tentativi di login negli ultimi 10 minuti! Attendere un po' di tempo prima di riprovare</p>
      <?php endif;?>
      <?php if($_GET['error'] == 'banned'): ?>
          <p>Questo account è stato sospeso.</p>
      <?php endif;?>
  </div>
  <?php endif;?>
  <p>Inserisci la tua email e password</p>
    <div class="form-group">
  		<label for="email" class="sr-only">Indirizzo email</label>
      <input name="email" type="email" id="email" class="form-control" placeholder="Indirizzo e-mail" required autofocus autocomplete="email">
    </div>
    <div class="form-group">
  		<label for="password" class="sr-only">Password</label>
      <input name="password" type="password" id="password" class="form-control" placeholder="Password" required autocomplete="current-password">
    </div>
    <div class="form-group form-check">
			<input type="checkbox" id="remember-me" value="remember-me" name="remember-me" class="form-check-input">
			<label for="remember-me" class="form-check-label">Ricordami</label>
    </div>
    <button class="btn btn-primary btn-lg btn-block" type="submit">Accedi</button>
    <a class="btn btn-warning btn-lg btn-block" href="/registrazione">Non hai un account? Registrati</a>
  </form>


<?=load_template('grid-end')?>
<?=load_template('end')?>
