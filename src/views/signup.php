<?=
load_template('head', [
  'title' => 'Registrati',
  'css' => [
    'auth.css'
  ]
])
?>

	<?=load_template('header')?>
    <?=load_template('auth-start')?>
       <h1>Registrati a UniDelivery</h1>
       <p>Inserisci i tuoi dati</p>
      <form method="post">
        <div class="form-group">
          <label for="name" class="sr-only">Nome e cognome</label>
          <input type="text" name="name" id="name" class="form-control" placeholder="Nome e cognome" pattern="[a-zA-Z ]{5,30}" required autofocus autocomplete="name">
          <div class="invalid-feedback">
            Da 5 a 30 caratteri: lettere e spazi (nome e cognome)
          </div>
        </div>
        <div class="form-group">
          <label for="email" class="sr-only">Email</label>
        <input type="email" name="email" id="email" class="form-control" placeholder="Indirizzo e-mail" required autocomplete="email" data-validate="server(/api/form-validation/assert-email-unregistered)">
        </div>
        <div class="form-group">
          <label for="password" class="sr-only">Password</label>
        <input type="password" name="password" id="password" class="form-control" placeholder="Password" autocomplete="new-password" required>
        </div>
        <div class="form-group">
          <label for="confirmpw" class="sr-only">Ripeti Password</label>
        <input type="password" id="confirmpw" class="form-control" placeholder="Conferma Password" required autocomplete="off" data-validate="sameAs(password)">
      </div>
        <div class="form-group form-check">
			<input type="checkbox" id="policies" value="policies" class="form-check-input" required>
			<label for="policies" class="form-check-label">Accetto i <a href="policies#terms">termini e le condizioni d'uso</a>. Ho letto l'<a href="policies#privacy">informativa sulla privacy</a> e l'<a href="policies#cookies">informativa sui cookie</a>.</label>
        </div>
        <button class="btn btn-primary btn-lg btn-block" type="submit">Iscriviti</button>
        <a class="btn btn-warning btn-lg btn-block" href="/login">Hai già un account? Accedi</a>
        <a class="btn btn-warning btn-lg btn-block" href="/registrazione-fornitore">Sei un fornitore? Registrati qui</a>
      </form>
<?=load_template('grid-end')?>

<?=load_template('end')?>
