<?=
load_template('head', [
  'title' => 'Account confermato',
  'css' => [
    'auth.css'
  ]
])
?>

<?=load_template('header')?>
<?=load_template('auth-start')?>
   <h1>Account confermato!</h1>
   <p>Procedi al <a href='/login'>login</a></p>
<?=load_template('grid-end')?>

<?=load_template('end')?>
