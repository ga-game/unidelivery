﻿<?=
load_template('head', [
  'title' => 'Policies',
  'css' => [
		'policies.css'
  ]
])
?>


	<?=load_template('header')?>

	<section id="terms" class="text-center">
<h1>Termini e Condizioni Generali di contratto e di utilizzo del sito</h1>

		<p>Le presenti condizioni generali di contratto disciplinano il rapporto negoziale tra Unidelivery srl, in persona del legale rappresentante pro tempore, con sede legale in Cesena, Via Fasulla 123, P.I. 07643520560, pec unidelivery@pec.it, (di seguito “Titolare” o “Gestore”) proprietaria del sito www.unidelivery.it, (di seguito Sito) e le persone fisiche, (di seguito “Utente” o “Utenti”) che, previa registrazione al Sito, ordinano prodotti alimentari agli esercizi commerciali presenti nel Sito (di seguito esercizi commerciali).</p>

<h2>ART. 1 - OGGETTO DEL CONTRATTO</h2>

<h3>1.1. Forma oggetto del presente accordo l'erogazione, da parte di Unidelivery srl a favore dell’Utente, dei servizi contenuti all'interno del Sito. In particolare, all'Utente iscritto, a seguito dell'accettazione del presente contratto e della Policy Privacy pubblicata sul Sito, viene riconosciuta la facoltà di:</h3>

<p>a) inoltrare ordini di acquisto (di seguito “Ordine”) e consegna a domicilio/ritiro di prodotti alimentari (di seguito "Prodotti") agli esercizi commerciali presenti nel Sito;</p>

<p>b) recensire gli esercizi commerciali presso i quali sia stato eseguito dall'Utente almeno un ordine di acquisto di Prodotti.</p>

<h3>1.2. Al fine di poter validamente utilizzare le funzionalità del Sito e dei servizi descritti, l’Utente sarà tenuto ad utilizzare un dispositivo connesso alla rete internet od un terminale mobile.</h3>

<h3>1.3. Eventuali migliorie tecniche e/o creative, che venissero realizzate dal Titolare durante il periodo di esecuzione del presente contratto saranno incluse all'interno dei Servizi erogati a favore degli Utenti.</h3>

<h3>1.4. Il titolare precisa espressamente – e l’Utente con l'approvazione del presente accordo, dichiara di accettare - come il presente contratto disciplini esclusivamente i rapporti nascenti dall’uso dello spazio web e che, per l'effetto, tale accordo non costituisca in alcun modo un contratto di compravendita, né un contratto avente ad oggetto un'obbligazione di risultato o di qualsivoglia altra natura. In ragione di ciò, pertanto, l'Utente dichiara di essere consapevole che Unidelivery srl si limiterà a mettere a disposizione di Utenti e Clienti il Sito, non avendo quindi alcuna responsabilità in ordine al sotteso rapporto contrattuale intervenuto tra l'Utente e l'esercizio commerciale.</h3>

<h2>ART. 2 - MODALITÀ DI STIPULAZIONE DEL CONTRATTO</h2>

<h3>2.1. Le parti precisano espressamente come il presente contratto si intenderà concluso esclusivamente attraverso la rete Internet, mediante l’accesso dell'Utente all’indirizzo www.unidelivery.it. L'Utente, a seguito della propria registrazione al Sito, effettuata seguendo le procedure guidate, potrà validamente usufruire dei Servizi erogati dal Gestore.</h3>

<h3>2.2. Il presente contratto si intenderà perfezionato a seguito:</h3>

<p>a) dell’esatta e completa compilazione del Form di registrazione dell'Utente;<p>

<p>b) dell'accettazione delle presenti Condizioni generali di contratto e della Privacy Policy del Sito;<p>

<p>c) della successiva convalida del recapito telefonico mediante inserimento della password inviata dal Gestore al numero di telefono cellulare indicato dall’Utente.<p>

<h2>ART. 3 - DURATA E RECESSO</h2>

<h3>3.1 Il presente accordo, salvo quanto previsto dal successivo ART. 8 (modifiche unilaterali), avrà una durata a tempo indeterminato ed avrà decorrenza dalla data di iscrizione dell'Utente al Sito.</h3>

<h3>3.2. All’Utente è consentito recedere dal presente contratto cliccando sul tasto “cancellami” presente nel proprio account. A seguito del ricevimento di tale comunicazione, il Gestore provvederà ad eliminare i dati dell’Utente nel minor tempo possibile.</h3>

<h2>ART. 4 - GRATUITA' DEL SERVIZIO</h2>

<h3>4.1. I Servizi di cui all'ART. 1 verranno resi da Unidelivery srl, a favore dell'Utente, in forma totalmente gratuita.</h3>

<h2>ART. 5 - MODALITÀ DI EMISSIONE E GESTIONE DELL’ORDINE</h2>

<h3>5.1. Nelle schede di ciascun esercizio commerciale sono indicati i tempi indicativi di consegna dei Prodotti, l’eventuale gratuità del servizio di consegna a domicilio, l’importo minimo dell’ordinativo e l’eventuale accettazione di buoni pasto da parte dell’esercizio commerciale.</h3>

<h3>5.2. Al fine di inoltrare validamente un ordine di acquisto, l’Utente, previa compilazione della procedura di iscrizione, dovrà:</h3>

<p>a) selezionare l’esercizio commerciale prescelto;</p>

<p>b) selezionare i Prodotti che intende ordinare;</p>

<p>c) verificare la correttezza dell’ordine ed il prezzo dei prodotti acquistati;</p>

<p>d) indicare il tempo di consegna desiderato ovvero l’orario di consegna nella sezione “pianifica consegna”. Si precisa che le tempistiche di consegna saranno visualizzate nel messaggio di conferma dell’ordine.</p>

<p>e) scegliere la modalità di pagamento (carta di credito/debito o pagamento alla consegna);</p>

<p>f) confermare l’Ordine;</p>

<p>g) attendere la conferma dell’ordine e l’accettazione da parte dell’esercizio commerciale che verrà notificata con il seguente messaggio “il tuo ordine è stato confermato, sarà consegnato entro (….) minuti”.</p>

<h3>5.3. Qualora l’esercizio commerciale rifiutasse l’Ordine, nella schermata comparirà il seguente messaggio  “l’ordine è stato rifiutato. L’esercizio commerciale è al momento occupato. Per favore riprova più tardi”, ed all’Utente non sarà addebitata alcuna spesa.</h3>

<h3>5.4. Nel caso in cui all’Utente, per errore, venisse addebitato il costo dei Prodotti nonostante il rifiuto dell’Ordine da parte dell’esercizio commerciale, l’Utente è tenuto ad eseguire uno screen-shot della schermata contenente il messaggio di rifiuto dell’ordine ed inviare all’indirizzo email info@unidelivery.it, entro e non oltre due ore dall’esecuzione dell’Ordine, lo screen-shot acquisito, unitamente alla prova dell’ingiustificato addebito. In tale ipotesi, a seguito delle verifiche del caso, il Gestore provvederà a rimborsare le somme erroneamente addebitate ovvero ad emettere un buono spesa di pari importo.</h3>

<h3>5.5. A seguito della conferma dell’Ordine, la transazione tra l’Utente e l’esercizio commerciale si intenderà validamente conclusa e, pertanto, a seguito di tale conferma l'Ordine non potrà esser successivamente modificato od annullato.</h3>

<h3>5.6. In caso di mancata autorizzazione al pagamento da parte dell’Istituto emittente la carta di credito, l’Ordine non verrà elaborato e non verrà quindi comunicato all'esercizio commerciale.</h3>

<h2>ART. 6. RECENSIONI</h2>

<h3>6.1. All’Utente è consentito rilasciare recensioni sulla qualità dei Prodotti acquistati e sul servizio prestato dagli esercizi commerciali.</h3>

<h3>6.2. Con la sottoscrizione del presente accordo l’Utente si obbliga a non rilasciare recensioni che, a titolo esemplificativo e non esaustivo:</h3>

<p>a) abbiano contenuto diffamatorio, osceno, offensivo, blasfemo o comunque inadatto ai minori;</p>

<p>b) promuovano violenza o discriminazione;</p>

<p>c) violino diritti di proprietà intellettuale di terzi;</p>

<p>d) siano finalizzati a promuovere attività illecite.</p>

<h3>6.3. Sebbene il Gestore non sia in alcun modo responsabile del contenuto delle recensioni rilasciate dagli Utenti, il Gestore medesimo si riserva comunque il diritto, senza assumere al riguardo alcuna obbligazione,  di rimuovere i commenti e le recensioni ritenute, a proprio insindacabile giudizio, illecite, diffamatorie o comunque lesive dei diritti di terzi.<h3>


<h2>ART. 7  - OBBLIGHI E RESPONSABILITA' DELL'UTENTE</h2>

<h3>7.1. Con la sottoscrizione del presente accordo, l'Utente:<h3>

<p>a) dichiara e garantisce di avere compiuto 18 anni e di possedere la capacità d’agire e di concludere contratti giuridicamente vincolanti;</p>

<p>b) dichiara di essere titolare della carta di credito mediante la quale esegue il pagamento dei Prodotti, consapevole delle sanzioni penali previste per il reato di indebito utilizzo di carte di credito o di pagamento di cui all’ART. 12 L 197/91 e ss. L'Utente infatti, sarà l'unico responsabile dell’indebito o fraudolento utilizzo del mezzo di pagamento.</p>

<p>c) si impegna a richiedere agli esercizi commerciali informazioni sulla composizione dei Prodotti nel caso di allergie od intolleranze alimentari,</p>

<p>d) garantisce la veridicità dei dati forniti in occasione della compilazione dei form di registrazione, consapevole delle sanzioni penali previste in relazione al rilascio di dichiarazioni mendaci, ai sensi e per gli effetti dell’ART. 46 DPR 445/2000. L'Utente infatti, sarà l'unico responsabile sia delle dichiarazioni false od inesatte dallo stesso fornite, che delle eventuali azioni che possano pregiudicare l'immagine del Gestore e/o di terzi;</p>

<p>e) si obbliga ad astenersi dal coinvolgere Unidelivery srl in eventuali contenziosi o controversie nascenti dai contratti conclusi tramite la propria pagina a causa della inesatta o mancata esecuzione della prestazione da parte degli esercizi commerciali;</p>

<p>f) dichiara di essere consapevole che Unidelivery srl non risulta, in alcun modo parte contrattuale del contratto di compravendita sottoscritto fra l'Utente e l’esercizio commerciale;</p>

<p>g)  dichiara che l'utilizzo dei servizi del Sito non sarà finalizzata a commettere attività illecite;</p>

<p>h) dichiara che l'utilizzo dei servizi del Sito non potrà compromettere in alcun modo l'immagine commerciale di Unidelivery srl Srl e/o di soggetti terzi.</p>

<p>i) si obbliga sin d’ora a tenere indenne Unidelivery srl Srl  da qualsivoglia richiesta risarcitoria, da chiunque proveniente, derivante dalla violazione degli articoli che precedono o di altra normativa a tutela di terzi.</p>

<p>l) si obbliga a non pubblicare sulle pagine del Sito recensioni che possano presentare contenuto di carattere osceno, blasfemo, offensivo, diffamatorio o comunque inadatto ai minori;</p>

<p>m) si obbliga a manlevare il Gestore da qualsiasi eventuale richiesta risarcitoria, tanto in sede giudiziale quanto in sede o stragiudiziale, avanzate da terzi nei confronti di Unidelivery srl Srl  a causa della pubblicazione da parte dell'Utente di recensioni aventi carattere diffamatorio o comunque lesivo dei diritti di terzi.</p>

<p>n) accetta di essere il solo responsabile di tutti gli utilizzi del proprio account.</p>

<h2>ART. 8 - ESONERO DI RESPONSABILITÀ DI Unidelivery SRL</h2>

<h3>8.1. Le parti danno reciprocamente atto che il presente contratto disciplini esclusivamente i rapporti nascenti dall’uso dello spazio web e che, per l'effetto, tale accordo non costituisca in alcun modo un contratto di compravendita, né un contratto avente ad oggetto un’obbligazione di risultato o di qualsivoglia altra natura, pertanto,  il Gestore non potrà essere ritenuto responsabile del ritardo o dell’inadempimento posto in essere dagli esercizi commerciali né, tanto meno, delle conseguenze derivanti da un siffatto ritardo/ inadempimento.</h3>

<h3>8.2. Il Gestore non garantisce che la qualità dei Prodotti ordinati ed il servizio fornito dagli esercizi commerciali risulti soddisfacente o che i Prodotti siano idonei, per qualità o per quantità, allo scopo per i quali sono stati ordinati, né garantisce l’osservanza dei tempi di consegna da parte degli esercizi commerciali.</h3>

<h3>8.3. L'Utente, con la sottoscrizione del presente accordo, accetta che Unidelivery srl venga espressamente esonerata da qualsivoglia responsabilità per:</h3>

<p>a) interruzione o sospensione dei Servizi, causati questi da problematiche di natura tecnica, da caso fortuito, forza maggiore, provvedimenti dell'Autorità giudiziaria, fatti di terzi e, in genere, da ogni impedimento od ostacolo che non possa essere superato con l'ordinaria diligenza;</p>

<p>b) eventuali conseguenze pregiudizievoli derivanti dal verificarsi di una o più circostanze di cui al punto a);</p>

<p>c) disservizi o malfunzionamenti connessi all’utilizzo della rete Internet indipendenti dal proprio operato e/o da quello propri fornitori;</p>

<p>d) false informazioni in ordine all'identità degli Utenti;</p>

<p>e) uso indebito e fraudolento delle carte di credito o di debito da parte degli Utenti;</p>

<p>f) l'operato e l'eventuale condotta inadempiente/negligente tenuta dall’esercizio commerciale. Resta infatti inteso come Unidelivery srl risulti totalmente estranea e quindi terza rispetto al contratto di compravendita stipulato tra l’Utente e l’esercizio commerciale.</p>

<p>g) eventuali intossicazioni alimentari, patologie derivanti dalla presenza di allergie od intolleranze alimentari derivanti dal consumo dei Prodotti acquisti e/o dall’ingestione di Prodotti avariati, contaminati da virus e batteri;</p>

<p>h) le recensioni rilasciate dagli Utenti non essendo Unidelivery srl tenuta a verificare il contenuto di cui ne declina ogni responsabilità;</p>

<p>i) l'eventuale sospensione temporanea del servizio, a causa di un evento derivante da interventi di miglioramento, riparazione e/o manutenzione della Piattaforma;</p>

<p>l) per il contenuto di qualsiasi altro sito web tramite il quale l’Utente abbia raggiunto il Sito o accessibili tramite link dal Sito.</p>

<h2>ART. 9 – SEGNALAZIONI E RECLAMI</h2>

<h3>9.1. Qualora l'Utente riscontrasse irregolarità nel funzionamento del Sito, oppure contenuti aventi carattere diffamatorio, denigratorio, oppure palesemente inattendibili/falsi ovvero che determinino una violazione di un altrui diritto, è tenuto darne tempestiva comunicazione a Unidelivery srl, tramite e-mail all’indirizzo info@unidelivery.it. Unidelivery srl, qualora reputasse fondata la segnalazione ricevuta, provvederà, entro un congruo termine, ad adottare gli opportuni rimedi.</h3>

<h2>ART. 10 -  PROPRIETA' INDUSTRIALE E INTELLETTUALE</h2>

<h3>10.1. I contenuti del Sito ed il marchio aziendale sono di proprietà di Unidelivery srl o licenziati a quest'ultima, risultando per l'effetto protetti dalle norme vigenti in materia di proprietà industriale ed intellettuale.</h3>

<h3>10.2 In caso di inosservanza dei diritti di proprietà industriale o intellettuale, l’Utente è tenuto a risarcire il danno arrecato a Unidelivery srl.</h3>

<h2>ART. 11 - CLAUSOLA RISOLUTIVA ESPRESSA</h2>

<h3>11.1. Le parti convengono espressamente che Unidelivery srl, ai sensi e per gli effeti dell’ART. 1456 c.c., potrà risolvere il presente contratto, in qualsiasi momento, senza preavviso e con effetto immediato, nel caso in cui, l'Utenti:</h3>

<p>a) fornisca, in sede di registrazione, informazioni e dati inesatti, incompleti o falsi relativi alla propria identità personale;</p>

<p>b) utilizzi o abbia utilizzato il Sito per commettere attività illecite o comunque, ad insindacabile giudizio del Gestore, incompatibili con la natura del Servizio fornito o lesive dell’immagine commerciale di Unidelivery srl e/o di soggetti terzi;</p>

<p>c) pubblichi recensioni aventi contenuto di carattere osceno, blasfemo, offensivo, diffamatorio o comunque contrario al buon costume ed alle norme di legge;</p>

<p>d) compia azioni potenzialmente idonee a danneggiare, disattivare, sovraccaricare, compromettere le funzionalità del Sito ovvero interferire con l’utilizzo dello stesso da parte di terzi;</p>

<h3>11.2. Nelle ipotesi previste dal comma precedente Unidelivery srl procederà, senza alcun preavviso, a disattivare l’account dell’Utente, nonchè a comunicare a quest’ultimo, a mezzo di sms o e-mail, l’avvenuta risoluzione del contratto e le modalità di ri-accredito delle somme eventualmente presenti nel relativo account.</h3>

<h3>11.3. Unidelivery srl si riserva sin d’ora di agire nei confronti dell’Utente per il risarcimento del danno causato dalla negligente condotta posta in essere da quest’ultimo.</h3>

<h2>ART. 12 - PRIVACY E COOKIE</h2>

<h3>12.1. Per informazioni sulle modalità di trattamento dei dati personali l’Utente è invitato a consultare la policy privacy  e la policy cookie del Sito.</h3>

<h2>ART. 13 - LEGGE APPLICABILE</h2>

<h3>13.1. Il presente contratto viene regolato dalla legge italiana.</h3>

<h2>ART. 14 MODIFICHE DELLE CONDIZIONI GENERALI DI UTILIZZO</h2>

<h3>14.1. Il Gestore si riserva il di modificare, in ogni momento e senza preavviso, le presenti condizioni generali di utilizzo. L’Utente è pertanto invitato a visitare periodicamente la presente pagina web.</h3>

<p>Qualora l'Utente  rifiutasse di aderire alle condizioni contenute all'interno delle presenti Condizioni generali di contratto, lo stesso è invitato a non sottoscrivere tale accordo.

Accettazione espressa

Ai sensi e per gli effetti di cui all’ART. 1341 c.c., sottoscrivendo il presente contratto, comprensivo di allegati, l'Utente dichiara di averne letto e compreso i contenuti e di accettarne integralmente le condizioni previste dagli artt. 2 (modalità di stipulazione del contrato), 3 (Durata e recesso), 5 (Modalità di emissione e gestione ordine) 6 (Recensioni), 7 (Credito account e buoni spesa) 8 (Obblighi e responsabilità dell'utente), 9 (Esonero di responsabilità Unidelivery Srl), 11 (Proprietà industriale ed intellettuale), 12(Clausola risolutiva espressa), 13 (Privacy e cookie), 14 (legge applicabile) e 15 (Modifiche delle condizioni generali di utilizzo).</p>

</section>
<section id="privacy">
		<h1>Informativa privacy ex D.lgs.196/03 e Regolamento UE nr. 679/2016</h1>

<p>Ai sensi dell’ART. 13 del D.lgs.196/03 ed in seguito all’entrata in vigore del Regolamento UE nr. 679/2016 conformemente a quanto disciplinato dall’ART. 13 del citato Regolamento Europeo, desideriamo comunicarVi quanto segue:</p>

<h2>Finalità del trattamento</h2>
<p>I Vs. dati personali, liberamente comunicati e da noi acquisiti in ragione dell’attività svolta da:
Unidelivery s.r.l.
Via Fasulla, 123
Partita IVA: 07643520560
Codice Fiscale: MRARSS80A01C573C

saranno trattati in modo lecito e secondo correttezza per le seguenti finalità:

Informazioni di registrazione relative all’apertura di un account, così da:

generare il tuo account al fine di consentirti di effettuare Ordini secondo le nostre Condizioni generali;

identificarti quando accedi al tuo account.

I dati trattati sono aggiornati, pertinenti, completi e non eccedenti rispetto alle finalità sopra elencate per le quali sono raccolti e successivamente trattati.</p>

<h2>Modalità del trattamento</h2>
<p>I dati medesimi verranno trattati, nel rispetto della sicurezza e riservatezza necessarie, attraverso le seguenti modalità: raccolta dei dati presso l’interessato,  raccolta  e registrate per scopi determinati, espliciti e legittimi, utilizzati in ulteriori operazioni di trattamento in termini compatibili con tali scopi, trattamento posto in essere con l’ausilio di strumenti elettronici ed automatizzati (raccolta dei dati per via telematica, direttamente presso l’interessato).</p>

<h2>Base giuridica del trattamento</h2>
<p>La base giuridica del trattamento dei Vs. dati personali si fonda sulla registrazione al portale web www.unidelivery.it.

Legittimi interessi perseguiti dal Titolare del Trattamento:
I legittimi interessi perseguiti dal Titolare del Trattamento nel trattamento dei dati è data dal dover rispettare ed onorare le obbligazioni contrattuali sottoscritte tra le parti. Ai sensi dell’ART. 6 la liceità del trattamento si basa sul consenso manifestamente espresso da parte dell’interessato, documentato in forma scritta. Natura obbligatoria o facoltativa del conferimento dei dati e conseguenze di un eventuale rifiuto a rispondere: la natura del conferimento dei dati da parte Vostra è obbligatoria affinché il titolare del trattamento possa erogare i servizi richiesti. In caso di rifiuto sarà quindi impossibile completare il processo di registrazione e il Titolare di Sistema non potrà adempiere agli obblighi contrattuali.</p>

<h2>Comunicazione dei dati a terzi</h2>
<p>I Vs. dati personali saranno trattati dal Titolare del trattamento, dai Responsabili del trattamento da lui nominati e dagli incaricati del trattamento strettamente autorizzati.  I Vs. dati personali non sono oggetto di diffusione.</p>

<h2>Tempi di conservazione</h2>
<p>I Vs. dati personali saranno conservati per nr. 10 anni, dalla cessazione del rapporto di erogazione del servizio/di fornitura del prodotto presso gli archivi informatici di proprietà di Unidelivery s.r.l..</p>

<h2>Intenzione del Titolare del trattamento dati personali</h2>
<p>Il Titolare del trattamento non trasferirà i Vs. dati personali ad un paese terzo o ad una organizzazione internazionale.</p>

<h2>Titolare e Responsabile del trattamento</h2>
<p>Titolare del trattamento dei dati è la Unidelivery s.r.l.. Dati contatti del Titolare del trattamento, mail : info@unidelivery.it

L’interessato potrà in qualsiasi momento esercitare i diritti a Lui riservati, sanciti dall’ART. 7 di cui si riporta il testo integrale: ART. 7 D.Lgs. 196/2003 e ART. 15 Regolamento UE 679/2016 - Diritto di accesso ai dati personali ed altri diritti – “L’interessato ha diritto di ottenere la conferma dell’esistenza o meno di dati personali che lo riguardano, anche se non ancora registrati, e la loro
comunicazione in forma intelligibile. L’interessato ha diritto di ottenere l’indicazione: dell’origine dei dati personali; delle finalità e modalità del trattamento; della logica applicata in caso di trattamento effettuato con l’ausilio di strumenti elettronici; degli estremi identificativi del titolare del trattamento, del responsabile e del rappresentante designato ai sensi dell’ART. 5 comma 2; dei soggetti o delle categorie di soggetti ai quali i dati personali possono essere comunicati o che possono venirne a conoscenza in qualità di rappresentante designato nel territorio dello Stato, di responsabili o incaricati. L’interessato ha  diritto di ottenere: l’aggiornamento, la rettificazione, ovvero, quando vi è interesse, l’integrazione dei dati; la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, compresi quelli di cui non è necessaria la conservazione in relazione agli scopi per i quali i dati sono stati raccolti o successivamente trattati; l’attestazione che le operazioni di cui alla lettera a. e b. sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi, eccettuato il caso in cui tale adempimento si rileva impossibile o comporta un impiego di mezzi manifestamente sproporzionato rispetto al diritto tutelato. L’interessato ha diritto di opporsi in tutto o in parte: per motivi legittimi al trattamento dei dati personali che lo riguardano, ancorché pertinenti allo scopo della raccolta; al trattamento dei dati personali che lo riguardano a fini di invio di materiale  pubblicitario o di vendita diretta o per il compimento di ricerche di mercato o di comunicazione commerciale”. In particolare l’interessato può in qualsiasi momento chiedere al Titolare del trattamento l’accesso ai dati personali e la rettifica o la cancellazione degli stessi o la limitazione del trattamento che lo riguardano o di opporsi al loro trattamento, oltre al diritto alla portabilità dei dati. L’interessato ha il diritto di revocare il consenso in qualsiasi momento senza pregiudicare la liceità del trattamento basata sul consenso prestato prima della revoca e ha il diritto di proporre reclamo a un’autorità di controllo. L’esercizio dei diritti può essere esercitato scrivendo all’indirizzo di posta elettronica info@unidelivery.it.</p>

</section>
<section id="cookies">
<h1>Informativa sui Cookie</h1>


<p>Data di entrata in vigore: December 04, 2018


Unidelivery ("noi" o "nostro") gestisce il www.unidelivery.it sito web (in appresso il "Servizio").

Questa pagina vi informa delle nostre politiche riguardanti la raccolta, l'uso e la divulgazione dei dati personali quando usate il nostro Servizio e le scelte che avete associato a quei dati. <a href="https://www.freeprivacypolicy.com/free-privacy-policy-generator.php">Informativa sulla Privacy via Free Privacy Policy Website</a>.

Utilizziamo i vostri dati per fornire e migliorare il Servizio. Utilizzando il Servizio, accettate la raccolta e l'utilizzo delle informazioni in conformità con questa informativa. Se non diversamente definito nella presente Informativa sulla privacy, i termini utilizzati nella presente Informativa hanno la stessa valenza dei nostri Termini e condizioni, accessibili da www.unidelivery.it</p>

<h2>Definizioni</h2>

    <p>
        <strong>Servizio</strong>
                Il Servizio è il sito www.unidelivery.it gestito da Unidelivery
            </p>
    <p>
        <strong>Dati personali</strong>
        I Dati personali sono i dati di un individuo vivente che può essere identificato da quei dati (o da quelli e altre informazioni in nostro possesso o che potrebbero venire in nostro possesso).
    </p>
    <p>
        <strong>Dati di utilizzo</strong>
        I dati di utilizzo sono i dati raccolti automaticamente generati dall'utilizzo del Servizio o dall'infrastruttura del Servizio stesso (ad esempio, la durata della visita di una pagina).
    </p>
    <p>
        <strong>Cookies</strong>
        I cookie sono piccoli file memorizzati sul vostro dispositivo (computer o dispositivo mobile).
    </p>


<h2>Raccolta e uso delle informazioni</h2>
<p>Raccogliamo diversi tipi di informazioni per vari scopi, per fornire e migliorare il nostro servizio.</p>

<h3>Tipologie di Dati raccolti</h3>

<h4>Dati personali</h4>
<p>Durante l'utilizzo del nostro Servizio, potremmo chiedervi di fornirci alcune informazioni di identificazione personale che possono essere utilizzate per contattarvi o identificarvi ("Dati personali"). Le informazioni di identificazione personale possono includere, ma non sono limitate a:</p>


    <p>Indirizzo email</p>    <p>Nome e cognome</p>            <p>Cookie e dati di utilizzo</p>


<h4>Dati di utilizzo</h4>

<p>Potremmo anche raccogliere informazioni su come l'utente accede e utilizza il Servizio ("Dati di utilizzo"). Questi Dati di utilizzo possono includere informazioni quali l'indirizzo del protocollo Internet del computer (ad es. Indirizzo IP), il tipo di browser, la versione del browser, le pagine del nostro servizio che si visita, l'ora e la data della visita, il tempo trascorso su tali pagine, identificatore unico del dispositivo e altri dati diagnostici.</p>

<h4>Tracciamento; dati dei cookie</h4>
<p>Utilizziamo cookie e tecnologie di tracciamento simili per tracciare l'attività sul nostro Servizio e conservare determinate informazioni.
I cookie sono file con una piccola quantità di dati che possono includere un identificatore univoco anonimo. I cookie vengono inviati al vostro browser da un sito web e memorizzati sul vostro dispositivo. Altre tecnologie di tracciamento utilizzate sono anche beacon, tag e script per raccogliere e tenere traccia delle informazioni e per migliorare e analizzare il nostro Servizio.
Potete chiedere al vostro browser di rifiutare tutti i cookie o di indicare quando viene inviato un cookie. Tuttavia, se non si accettano i cookie, potrebbe non essere possibile utilizzare alcune parti del nostro Servizio.
Esempi di cookie che utilizziamo:</p>

    <p><strong>Cookie di sessione.</strong> Utilizziamo i cookie di sessione per gestire il nostro servizio.</p>
    <p><strong>Cookie di preferenza.</strong> Utilizziamo i cookie di preferenza per ricordare le vostre preferenze e varie impostazioni.</p>
    <p><strong>Cookie di sicurezza.</strong> Utilizziamo i cookie di sicurezza per motivi di sicurezza.</p>


<h2>Uso dei dati</h2>
<p>Unidelivery utilizza i dati raccolti per vari scopi:</p>

    <p>Per fornire e mantenere il nostro Servizio</p>
    <p>Per comunicare agli utenti variazioni apportate al servizio che offriamo</p>
    <p>Per permettere agli utenti di fruire, a propria discrezione, di funzioni interattive del nostro servizio</p>
    <p>Per fornire un servizio ai clienti</p>
    <p>Per raccogliere analisi o informazioni preziose in modo da poter migliorare il nostro Servizio</p>
    <p>Per monitorare l'utilizzo del nostro Servizio</p>
    <p>Per rilevare, prevenire e affrontare problemi tecnici</p>


<h2>Trasferimento dei dati</h2>
<p>Le vostre informazioni, compresi i Dati personali, possono essere trasferite a - e mantenute su - computer situati al di fuori del vostro stato, provincia, nazione o altra giurisdizione governativa dove le leggi sulla protezione dei dati possono essere diverse da quelle della vostra giurisdizione.
Se ci si trova al di fuori di Italy e si sceglie di fornire informazioni a noi, si ricorda che trasferiamo i dati, compresi i dati personali, in Italy e li elaboriamo lì.
Il vostro consenso alla presente Informativa sulla privacy seguito dall'invio di tali informazioni rappresenta il vostro consenso al trasferimento.
Unidelivery adotterà tutte le misure ragionevolmente necessarie per garantire che i vostri dati siano trattati in modo sicuro e in conformità con la presente Informativa sulla privacy e nessun trasferimento dei vostri Dati Personali sarà effettuato a un'organizzazione o a un paese a meno che non vi siano controlli adeguati dei vostri dati e altre informazioni personali.</p>

<h2>Divulgazione di dati</h2>

<h3>Prescrizioni di legge</h3>
<p>Unidelivery può divulgare i vostri Dati personali in buona fede, ritenendo che tale azione sia necessaria per:</p>

    <p>Rispettare un obbligo legale</p>
    <p>Proteggere e difendere i diritti o la proprietà di Unidelivery</p>
    <p>Prevenire o investigare possibili illeciti in relazione al Servizio</p>
    <p>Proteggere la sicurezza personale degli utenti del Servizio o del pubblico</p>
    <p>Proteggere contro la responsabilità legale</p>


<h2>Sicurezza dei dati</h2>
<p>La sicurezza dei vostri dati è importante per noi, ma ricordate che nessun metodo di trasmissione su Internet o metodo di archiviazione elettronica è sicuro al 100%. Pertanto, anche se adotteremo ogni mezzo commercialmente accettabile per proteggere i vostri Dati personali, non possiamo garantirne la sicurezza assoluta.</p>

<h2>Fornitori di servizi</h2>
<p>Potremmo impiegare società e individui di terze parti per facilitare il nostro Servizio ("Fornitori di servizi"), per fornire il Servizio per nostro conto, per eseguire servizi relativi ai Servizi o per aiutarci ad analizzare come viene utilizzato il nostro Servizio.
Le terze parti hanno accesso ai vostri Dati personali solo per eseguire queste attività per nostro conto e sono obbligate a non rivelarle o utilizzarle per altri scopi.</p>



<h2>Link ad altri siti</h2>
<p>Il nostro servizio può contenere collegamenti ad altri siti non gestiti da noi. Cliccando su un link di terze parti, sarete indirizzati al sito di quella terza parte. Ti consigliamo vivamente di rivedere l'Informativa sulla privacy di ogni sito che visiti.
Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per il contenuto, le politiche sulla privacy o le pratiche di qualsiasi sito o servizio di terzi.</p>


<h2>Privacy dei minori</h2>
<p>Il nostro servizio non si rivolge a minori di 18 anni ("Bambini").
Non raccogliamo consapevolmente informazioni personali relative a utenti di età inferiore a 18 anni. Se siete un genitore o tutore e siete consapevoli che vostro figlio ci ha fornito Dati personali, vi preghiamo di contattarci. Se veniamo a conoscenza del fatto che abbiamo raccolto Dati personali da minori senza la verifica del consenso dei genitori, adotteremo provvedimenti per rimuovere tali informazioni dai nostri server.</p>


<h2>Modifiche alla presente informativa sulla privacy</h2>
<p>Potremmo aggiornare periodicamente la nostra Informativa sulla privacy. Ti informeremo di eventuali modifiche pubblicando la nuova Informativa sulla privacy in questa pagina.
Vi informeremo via e-mail e / o un avviso di rilievo sul nostro Servizio, prima che la modifica diventi effettiva e aggiorneremo la "data di validità" nella parte superiore di questa Informativa sulla privacy.
Si consiglia di rivedere periodicamente la presente Informativa sulla privacy per eventuali modifiche. Le modifiche a tale informativa sulla privacy entrano in vigore nel momento in cui vengono pubblicate su questa pagina.</p>


<h2>Contattaci</h2>
<p>In caso di domande sulla presente Informativa sulla privacy, si prega di contattarci tramite e-mail: info@unidelivery.it</p>
</section>
  <?=load_template('footer')?>


<?=load_template('end')?>
