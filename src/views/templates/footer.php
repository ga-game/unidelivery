<footer class="page-footer font-small bg-light pt-4">
    <div class="container-fluid text-center text-md-left">
      <div class="row justify-content-center">
        <div class="col-sm-12 col-md-4 col-lg-3">
          <span class="h5 text-uppercase">UniDelivery s.r.l.</span>
          <p class="mt-2 mb-2 text-muted font-weight-bold">P.IVA 07643520560</p>
        </div>
        <hr class="clearfix w-100 d-md-none pb-3">
        <div class="col-sm-12 col-md-4 col-lg-3">
            <span class="h5 text-uppercase">Link</span>
            <ul class="list-unstyled">
              <li>
                <p class="mt-2 mb-2 text-muted font-weight-bold">Sei un fornitore? <a href="/registrazione-fornitore">Registrati qui</a></p>
              </li>
              <li>
                <p class="mt-2 mb-2 text-muted font-weight-bold">Leggi i nostri <a href="/policies#terms">termini e condizioni d'uso</a></p>
              </li>
              <li>
                <p class="mt-2 mb-2 text-muted font-weight-bold">La nostra <a href="/policies#privacy">informativa sulla privacy</a></p>
              </li>
              <li>
                <p class="mt-2 mb-2 text-muted font-weight-bold">La nostra <a href="/policies#cookies">informativa sui cookie</a></p>
            </ul>
          </div>
          <div class="col-sm-12 col-md-4 col-lg-3">
            <span class="h5 text-uppercase">Contatti</span>
            <ul class="list-unstyled">
              <li>
                <p class="mt-2 mb-2 text-muted font-weight-bold">Hai bisogno di aiuto o informazioni? Scrivici a <a href="mailto:info.unidelivery@gmail.com?subject=Richiesta%20informazioni">info.unidelivery@gmail.com</a></p>
              </li>
            </ul>
          </div>
      </div>
    </div>
    <div class="footer-copyright text-center py-3">
      <p class="mt-2 mb-3 text-muted">&copy; 2018</p>
    </div>
  </footer>
