<header>
		<nav class="navbar navbar-expand-md navbar-light fixed-top">
			<div class="container-fluid">
				<a class="navbar-brand font-weight-bold" href="/">
					<img src="/res/icon.jpg" alt="Logo" width="50" height="50">
					<span class="logo-name">UniDelivery</span>
				</a>
				<button class="navbar-toggler collapsed ml-auto" type="button" data-toggle="collapse" data-target="#navbar-toggle" aria-controls="navbar-toggle" aria-expanded="false">
					<span class="burger-icon"><span></span><span></span><span></span></span>
				</button>
				<div class="collapse navbar-collapse" id="navbar-toggle">
					<div class="navbar-nav ml-auto">
						<?php if(!Auth::get()): ?>
							<?php if($_SERVER['REQUEST_URI'] !== '/login'): ?>
									<a class="nav-item nav-link" href="/login">Accedi</a>
							<?php endif;?>
							<?php if(substr($_SERVER['REQUEST_URI'], 0, strlen('/registrazione')) !== '/registrazione'): ?>
									<a class="nav-item nav-link" href="/registrazione">Registrati</a>
							<?php endif;?>
						<?php else: ?>
							<a class="nav-item nav-link" href="/area-personale">Ciao, <?=Auth::get()->shortName?></a>
							<a class="nav-item nav-link" href="/notifiche">Notifiche</a>
							<?php if(Auth::get()->isSupplier()): ?>
								<a class="nav-item nav-link" href="/ristoranti/<?=Auth::get()->slug?>">Il mio ristorante</a>
								<a class="nav-item nav-link" href="/modifica-listino">Modifica listino</a>
							<?php endif; ?>
							<?php if(Auth::get()->isAdmin()): ?>
								<a class="nav-item nav-link" href="/admin">Pannello amministratore</a>
							<?php endif; ?>
							<form method="post" action="/logout"><input type="submit" class="btn btn-link nav-item nav-link" value="Logout"/></form>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</nav>
	</header>
	<script src="/js/header.js"></script>
