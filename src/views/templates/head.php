<!DOCTYPE html>
<html lang="it">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Ordinare cibo online direttamente in aula">
  <meta name="author" content="Federico Pettinari, Alessandro Oliva, Andrea Betti">
  <link rel="shortcut icon" href="/favicon.ico">

  <title><?=$title?></title>

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" integrity="sha384-1UXhfqyOyO+W+XsGhiIFwwD3hsaHRz2XDGMle3b8bXPH5+cMsXVShDoHA3AH/y/p" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" integrity="sha384-GHUTcUk7YeeUFGZAENEhudvXVYlTLSOMku/2BivswbwdUSldZVO4ovkP93xAd4Gs" crossorigin="anonymous">
  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"> -->

  <!-- JQuery and Bootstrap JavaScript files -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.5.0/js/all.js" integrity="sha384-GqVMZRt5Gn7tB9D9q7ONtcp4gtHIUEW/yG7h98J7IpE3kpi+srfFyyB/04OV6pG0" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" integrity="sha384-rgWRqC0OFPisxlUvl332tiM/qmaNxnlY46eksSZD84t+s2vZlqGeHrncwIRX7CGp" crossorigin="anonymous"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js" integrity="sha384-utW62Q5udTycRsqDMdQwjeaKASTAE2cf20juuz5yfC1n1hu8gBJ1Pn0oEzKIb8Gd" crossorigin="anonymous"></script>

  <!-- Custom scripts -->
  <script src="/js/sha512.min.js"></script>
  <script defer async src="/js/form-validator.js"></script>
  <script defer async src="/js/timeago.js"></script>
  <script defer async src="/js/htmlvalidator.js"></script>
  <?php if(!is_null(\Auth::get())): ?>
  <script defer async src="/js/get-notifications.js"></script>
  <?php endif;?>
  <?php foreach(isset($js) && is_array($js) ? $js : [] as $link): ?>
    <script src="/js/<?=$link?>"></script>
  <?php endforeach;?>

  <!-- Custom styles -->
  <link href="/css/base.css" rel="stylesheet">
  <?php foreach(isset($css) && is_array($css) ? $css : [] as $link): ?>
    <link href="/css/<?=$link?>" rel="stylesheet">
  <?php endforeach;?>

</head>
<body>


  <aside id="validator-dialog">
    <div class="group">
      <button type="button" class="btn btn-info btn-block" name="achecker">AChecker</button>
      <span><span class="fas fa-spinner fa-spin"></span></span>
    </div>
    <div class="group">
      <button type="button" class="btn btn-info btn-block" name="w3">W3</button>
      <span><span class="fas fa-spinner fa-spin"></span></span>
    </div>
  </aside>
