<?=
load_template('head', [
  'title' => 'Pagamento completato',
  'js' => [
    'home-redirect.js'
  ],
  'css' => [
    'auth.css'
  ]
])
?>

<?=load_template('header')?>

<?=load_template('auth-start')?>

<h1>Pagamento avvenuto con successo</h1>
<p>Se il reindirizzamento automatico non avviene entro 10 secondi, clicca <a href="/">qui</a>.<p>

<?=load_template('grid-end')?>

<?=load_template('end')?>
