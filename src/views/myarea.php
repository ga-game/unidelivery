<?=
load_template('head', [
  'title' => 'Area personale',
  'css' => [
    'myarea.css'
  ]
])
?>

  <?=load_template('header')?>

  <?=load_template('order-start')?>

    <h1>Area Personale</h1>
    <p>Qui puoi modificare i tuoi dati</p>
    <form method="post">
        <div class="form-group">
					<label for="name" class="field">Nome e cognome</label><label class="req">*</label>
          <input name="name" type="text" value="<?=$user->name?>" id="name" class="form-control" placeholder="Nome" required>
        </div>
        <div class="form-group">
					<label for="email" class="field">E-mail</label><label class="req">*</label>
          <input name="email" value="<?=$user->email?>" type="email" id="email" class="form-control" placeholder="Indirizzo e-mail" required>
        </div>
        <div class="form-group">
          <label for="phone" class="field">Telefono</label>
          <input name="phone" value="<?=$user->phone?>" type="number" id="phone" class="form-control" placeholder="Telefono">
        </div>
        <label class="req">*Campi obbligatori</label>
		<input class="btn btn-primary btn-lg" type="submit" value="Salva Modifiche">
	</form>

<?=load_template('grid-end')?>

  <?=load_template('footer')?>
<?=load_template('end')?>
