<?=
load_template('head', [
  'title' => $restaurant->name,
  'css' => [
    'restaurant-menu.css'
  ],
  'js' => [
    'restaurant-menu.js'
  ]
])
?>

  <?=load_template('header')?>

  <?=load_template('order-start')?>

  <div class="row" id="custom-search-input">
    <div class="col col-12">
    <form method="get" action="/ristoranti">
      <div class="input-group-search">
	  	  <label for="search" class="sr-only">Cerchi altro?</label>
		    <input id="search" name="cerca" class="form-control btn-lg" type="text" placeholder="Vuoi ordinare da altri?" value="<?=$query?>">
        <span class="input-group-btn-search">
          <input type="submit" class="btn-search" value="submit">
        </span>
      </div>
    </form>
  </div>
  </div>
	<div class="row" id="subheader">
		<div id="content">
			<img id="icon" src="<?=$restaurant->logoPath?>" alt="Logo ristorante">
			<h2><?=$restaurant->name?></h2>
			<p><?php foreach(db_query('SELECT tag FROM supplier_tagged WHERE supplier=?', [$restaurant->id]) as $tag) echo $tag->tag . " " ?></p>
			<p><?=$restaurant->address?> - <?=$restaurant->phone?></p>
		</div>
    <div id="background">
      <img id="cover" src="<?=$restaurant->imgPath?>" alt="Sfondo ristorante">
    </div>
	</div>


	<div class="row">
			<div id="menu" class="col-sm-12 col-lg-8">
        <div class="card mb-2">
           <div class="card-header">
              <h2 class="card-title">Men&ugrave;</h2>
           </div>
           <div class="card-body">
				<ol>
					<?php foreach($restaurant->getMenu() as $section): ?>
					<li class="section">
						<button class="section-name btn mb-1"><span><?=$section->name?><span class='fas fa-chevron-down'></span></span></button>
						<ol>
							<?php foreach($section->products as $product): ?>
							<li class="product">
								<div class="container">
									<div class="row align-items-center">
									<span class="product-id"><?=$product->id?></span>
									<span class="product-name col-6"><?=$product->name?></span>
										<span class="product-price col-4"><?=$product->price?></span>
										<button class="add-product btn btn-link col-2" aria-label="Aggiungi prodotto"><span class='far fa-plus-square'></span></button>
									</div>
								</div>
							</li>
							<?php endforeach; ?>
						</ol>
					</li>
					<?php endforeach; ?>
				</ol>
      </div>
			</div>
    </div>

      <div id="cart" class="col-sm-12 col-lg-4">
      <div class="card mb-2">
         <div class="card-header">
            <h2 class="card-title">Carrello</h2>
         </div>
         <div class="card-body">
				<ol class="mx-2">
				    <?php foreach($chart as $item):?>
					<li class='product'>
						<div class='container'>
							<div id='product-id-<?=$item['product']->id?>' class='row align-items-center'>
								<button class='remove-product btn btn-link col-2' aria-label='Rimuovi prodotto'><span class='far fa-minus-square'></span></button>
								<span class='product-id'><?=$item['product']->id?></span>
								<span class='product-quantity col-2'><?=$item['quantity']?></span> ×
								<span class='product-name col'><?=$item['product']->name?></span>
								<span class='product-price col'><?=$item['product']->price?></span>
							</div>
						</div>
					</li>
					<?php endforeach;?>
				</ol>
				<p>Subtotale: <span id="subtotal"><?=$totalPrice?></span></p>
				<p>Costo di consegna: <span id="delivery-cost">2.00</span></p>
				<p>Totale: <span id="total"><?=$totalPrice+2?></span></p>
				<form method="post" action="/ordina">
					<input type="hidden" id="supplier-slug" name="supplier-slug" value="<?=$restaurant->slug?>">
					<div class="d-none js-add-cart-form-here">
					</div>
					<button class="btn btn-block btn-primary btn-lg" type="submit" value="order" disabled>Ordina</button>
				</form>
			</div>
    </div>

			<div id="toast-container"></div>

		</div>
	</div>
<?=load_template('grid-end')?>

<?=load_template('footer')?>


<?=load_template('end')?>
