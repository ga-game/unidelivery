<?=
   load_template('head', [
     'title' => 'Pagamento',
     'css' => [
       'order-payment.css'
     ]
   ])
   ?>
<?=load_template('header')?>
<main class="container my-sm-1 my-md-2 my-lg-5">
<form method="post">
   <div class="row mb-2">
           <div class="col col-12">
            <section class="card">
               <div class="card-header">
                  <h2 class="card-title">Riepilogo ordine</h2>
               </div>
               <div class="card-body">
                  <div class="form-row">
                     <div class="form-group">
                        <ol>
                           <?php foreach($chart as $item):?>
                           <li class="nostyle"><?=$item['quantity']?> x <?=$item['product']->name?> €<?=$item['product']->price?></li>
                           <?php endforeach;?>
                        </ol>
                        <p>Subtotale: €<?=$subtotal?></p>
                        <p>Costo di consegna: €2</p>
                        <p>Totale: €<?=$totalPrice?></p>
                     </div>
                  </div>
               </div>
            </section>
          </div>
    </div>
    <div class="row mb-2">
           <div class="col col-12">
            <section class="card">
               <div class="card-header">
                  <h2 class="card-title">Consegna</h2>
               </div>
               <div class="card-body">
                        <fieldset>
                           <legend class="sr-only">Luogo e data di consegna:</legend>
                           <div class="form-row">
                           <div class="form-group">
                           <label for="location">Luogo</label>
                           <select class="form-control" id="location" name="location">
                              <?php foreach($locations as $location): ?>
                              <option value="<?=$location->name?>"><?=$location->name?></option>
                              <?php endforeach; ?>
                           </select>
                          </div>
                         </div>
                         <div class="form-row">
                          <div class="form-group">
                           <label for="date-delivery-day">Data</label>
                           <input type="date" class="form-control" id="date-delivery-day" name="date-delivery-day" value="<?=date("Y-m-d")?>" min="<?=date("Y-m-d")?>" required data-validate="afterNow">
                          </div>
                         </div>
                         <div class="form-row">
                          <div class="form-group">
                           <label for="date-delivery-time">Ore</label>
                           <input type="time" class="form-control" id="date-delivery-time" name="date-delivery-time" value="<?=date("H:i")?>" required data-validate="afterNowWithDate(#date-delivery-day)">
                          </div>
                          <input type="hidden" name="supplier-slug" value=<?=$supplier_slug?>>
                        </fieldset>
                  </div>
                 </div>
               </div>
            </section>
          </div>
         </div>
         <div class="row mb-2">
      <div class="col-sm-12">
         <section class="card mb-2">
            <div class="card-header">
               <h2 class="card-title">Dati di pagamento</h2>
               <img class="img-responsive" alt="Accettiamo visa, mastercard, american express, discover" src="http://i76.imgup.net/accepted_c22e0.png">
            </div>
            <div class="card-body">
               <div class="form-row payment">
                  <div class="form-group">
                     <label for="cardNumber">Numero carta</label>
                     <div class="input-group">
                        <input type="text" class="form-control" name="cardNumber" id="cardNumber" autocomplete="cc-number" required autofocus/>
                        <div class="input-group-append"><span class="input-group-text"><span class="fa fa-credit-card"></span></span></div>
                     </div>
                  </div>
               </div>
               <div class="form-row payment">
                  <div class="form-group">
                     <label for="cardExpiry">Data di scadenza</label>
                     <input type="month" class="form-control" name="cardExpiry" id="cardExpiry" placeholder="MM/AA" min=<?=date("Y-m")?> autocomplete="cc-exp" required />
                  </div>
               </div>
               <div class="form-row payment">
                  <div class="form-group">
                     <label for="cardCVC">Codice CV</label>
                     <input type="text" class="form-control" name="cardCVC" id="cardCVC" placeholder="CVC" autocomplete="cc-csc" required/>
                  </div>
               </div>
            </div>
         </section>
         <div class="text-center">
           <button type="submit" class="btn btn-primary btn-lg">Conferma ordine</button>
         </div>
      </div>
   </div>
</form>
</main>
<?=load_template('footer')?>
<?=load_template('end')?>
