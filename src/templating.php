<?php
/*
 * Funzioni per generazione pagine html da php
**/
require_once('config.php');
require_once('utils.php');

function load_view($path, array $parameters = []){
  foreach($parameters as $key => $value){
    ${$key} = $value;
  }
  require(VIEWS_PATH . $path . '.php');
}

function load_template($path, array $parameters = []){
  load_view(TEMPLATES_PATH . $path, $parameters);
}

?>
