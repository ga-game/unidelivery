<?php
namespace Controllers;

class NotificationController extends Controller {

  public function getNotifications() {
    $user = \Auth::require();

	db_exec("UPDATE Notification SET dateRead='" . date("Y-m-d H:i:s") . "' WHERE destination=?", [$user->id]);
    if ($user->isSupplier()) {
		load_view('order-list', []);
	} else {
		load_view('notifications', []);
	}
  }
  
    public function postNotifications() {
    $user = \Auth::require();

    if ($user->isSupplier()) {
		load_view('order-list', []);
	} else {
		load_view('notifications', []);
	}
  }
  
}