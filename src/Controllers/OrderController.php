<?php
namespace Controllers;
use \Models\Product as Product;
use \Models\Order as Order;
use \Models\Supplier as Supplier;
use \Models\Notification as Notification;

class OrderController extends Controller {
  public function postStartOrder() {
    $user = \Auth::get();
    $_SESSION['order-request-' . $_REQUEST["supplier-slug"]] = $_POST;
    if(is_null($user)){
      AccessController::setLoginHandler($this);
      redirect('/login');
    } else {
      redirect('/pagamento/' . $_REQUEST["supplier-slug"]);
    }
  }

  public function onLogin($user) {
        redirect_back();
  }

  public function getPayment($slug) {
	  if (isset($_SESSION['order-request-' . $slug])) {
		$data = $_SESSION['order-request-' . $slug];
		$pids = array_keys($data['product']);
		$prods = Product::findByIds($pids);
	  } else {
		$data = [];
		$prods = [];
	  }
    $price = 0;
    $chart = [];
    foreach($prods as $prod){
      $price += $prod->price * $data['product'][$prod->id];
      $chart[] = ['product' => $prod, 'quantity' => $data['product'][$prod->id]];
    }

    load_view('order-payment', [
      'chart' => $chart,
	  'subtotal' => $price,
      'totalPrice' => $price + 2,
	  'locations' => db_query('SELECT name FROM Location'),
	  'supplier_slug' => $slug
    ]);
  }

  public function postPayment() {
    $user = \Auth::require();
    $data = $_SESSION['order-request-' . $_REQUEST["supplier-slug"]];
    $pid = array_keys($data['product'])[0];
    $supplier = Supplier::findByProduct($pid);
    $order = Order::create([
      'customer' => $user->id,
	  'dateDelivery' => $_REQUEST['date-delivery-day'] . ' ' . $_REQUEST['date-delivery-time'],
	  'datePayment' => date("Y-m-d H:i:s"),
      'location' => $_REQUEST['location'],
      'status' => 'queued',
      'products' => $data['product'],
      'comment' => ''
    ]);

    Notification::create([
      'destination' => $supplier->id,
      'order' => $order->id,
      'type' => 'order-created'
    ]);
	
	$orderLink = 'http://'.$_SERVER['HTTP_HOST'] . '/notifiche#ordine' . $order->id;
	$mail = new \Mail('UniDelivery: Ordine ricevuto', [
      "È stato ricevuto l'ordine $order->id. \r\nPer visualizzare i dettagli, visita la pagina degli ordini:\r\n $orderLink",
      "È stato ricevuto l'ordine $order->id. </br>Per visualizzare i dettagli, clicca su <strong><a href=\"$orderLink\">questo link</a></strong>"]);
    $mail->sendTo($supplier->email);
	
	unset($_SESSION['order-request-' . $_REQUEST["supplier-slug"]]);
    
    load_view('payment-complete');
  }
}
