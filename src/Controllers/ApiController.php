<?php
namespace Controllers;

class ApiController extends JsonController {
  public function getNotifications() {
    $user = \Auth::require();
    $sinceId = request_value('since', null);
    if(is_string($sinceId)) {
		if ($sinceId === "all") {
			$response = \Models\Notification::all($user);
		} else {
			$response = \Models\Notification::since($user, $sinceId);
		}
    } else {
      $response = \Models\Notification::unread($user);
    }
    echo json_encode($response);
  }

  public function postNotification($id) {
		if (isset($_REQUEST['dateRead'])) {
			$date = $_REQUEST['dateRead'];
			$q = db_exec('UPDATE Notification SET dateRead=? WHERE id=?', [$date, $id]);
		}
		if (isset($_REQUEST['comment'])) {
			$comment = $_REQUEST['comment'];
			$q = db_exec('UPDATE `order` SET comment=?, status="sent" WHERE id=?', [$comment, $id]);

			$customer = \Models\User::findByOrder($id);
			$notification = \Models\Notification::create([
				'destination' => $customer->id,
				'order' => $id,
				'type' => 'order-sent'
			]);

			$q = db_query('SELECT U.* FROM User U, Order_product O, Product P, Menusection M WHERE O.order=? AND P.id = O.product AND P.section = M.id AND M.supplier = U.id LIMIT 1', [$id]);
			
			$orderLink = 'http://'.$_SERVER['HTTP_HOST'] . '/notifiche#ordine' . $id;
			foreach($q as $supplier) {
				$mail = new \Mail('UniDelivery: il tuo ordine è stato spedito', [
					"$supplier->name ha spedito il tuo ordine. \r\nPer visualizzare i dettagli, visita la pagina delle notifiche:\r\n $orderLink",
					"$supplier->name ha spedito il tuo ordine. </br>Per visualizzare i dettagli, clicca su <strong><a href=\"$orderLink\">questo link</a></strong>"]);
			}
			
			$mail->sendTo($customer->email);
		}
  }

  public function getUsers() {
  	$response = db_query("SELECT id,address,banned,confirmationCode,email,id,imgPath,logoPath,name,phone,rememberToken,role,shortName,slug, CONCAT(LEFT(password, 10), IF(LENGTH(password)>10, '…', '')) AS password FROM User");
  	echo json_encode($response);
  }

  public function getSupplierTags() {
  	$response = db_query("SELECT * FROM Suppliertag");
  	echo json_encode($response);
  }
}
