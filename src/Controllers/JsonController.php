<?php
namespace Controllers;

abstract class JsonController extends Controller {
  protected const TRACK_PREVIOUS_URL = false;

  public function __construct() {
    parent::__construct();
    header('Content-Type: application/json');
  }

}
