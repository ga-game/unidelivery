<?php
namespace Controllers;

class MyAreaController extends Controller {

  public function getUser() {
    $user = \Auth::require();

    if ($user->isSupplier()) {
		load_view('myarea-supplier', ['user' => $user]);
	} else {
		load_view('myarea', ['user' => $user]);
	}
  }

  public function postUser() {
    $user = \Auth::require();
	if ($user->isSupplier()) {
		$q = db_exec('UPDATE User SET name=?, shortName=?, email=?, address=?, phone=? WHERE id=?', [$_REQUEST["name"], strtok($_REQUEST['name'], ' '), $_REQUEST["email"], $_REQUEST["address"], $_REQUEST["phone"], $user->id]);
		$user->name = $_REQUEST["name"];
		$user->shortName = strtok($_REQUEST['name'], ' ');
		$user->email = $_REQUEST["email"];
		$user->address = $_REQUEST["address"];
		$user->phone = $_REQUEST["phone"];

    foreach(db_query('SELECT * FROM suppliertag', []) as $tag) {
      if (isset($_POST[$tag->name])) {
        if (empty(db_query('SELECT * FROM supplier_tagged WHERE supplier=? AND tag=?', [$user->id, $tag->name]))) {
          $q = db_exec('INSERT INTO supplier_tagged (supplier, tag) VALUES (?, ?)', [$user->id, $tag->name]);
        }
      } else {
        $q = db_exec('DELETE FROM supplier_tagged WHERE supplier=? AND tag=?', [$user->id, $tag->name]);
      }
    }

		if (!empty($_FILES["icon"]["name"])) {
			$q = db_exec('UPDATE User SET logoPath=? WHERE id=?', ["/" . $this->setImage("res/supplier-icons", basename($_FILES["icon"]["name"]), "icon", $user), $user->id]);
		}

		if (!empty($_FILES["cover"]["name"])) {
		  $q = db_exec('UPDATE User SET imgPath=? WHERE id=?', ["/" . $this->setImage("res/supplier-covers", basename($_FILES["cover"]["name"]), "cover", $user), $user->id]);
		}

		load_view('myarea-supplier', ['user' => $user]);
		} else {
			$q = db_exec('UPDATE User SET name=?, shortName=?, email=?, phone=? WHERE id=?', [$_REQUEST["name"], strtok($_REQUEST['name'], ' '), $_REQUEST["email"], $_REQUEST["phone"], $user->id]);
			$user->name = $_REQUEST["name"];
			$user->shortName = strtok($_REQUEST['name'], ' ');
			$user->email = $_REQUEST["email"];
			$user->phone = $_REQUEST["phone"];
			load_view('myarea', ['user' => $user]);
		}
  }

	function setImage($dir, $target_file, $type, $user)
	{
	    $uploadOk = 1;
	    $imageFileType = strtolower(pathinfo($dir . $target_file, PATHINFO_EXTENSION));
	    // Check if image file is a actual image or fake image
	    if (isset($_POST["submit"])) {
	        $check = getimagesize($_FILES[$type]["tmp_name"]);
	        if ($check !== false) {
	            $uploadOk = 1;
	        } else {
	            echo "<script type='text/javascript'>alert('Il file non è un immagine!')</script>";
	            $uploadOk = 0;
	        }
	    }
	    // Check if file already exists
	    /*if (file_exists($target_file)) {
	        echo "<script type='text/javascript'>alert('Il file caricato è già in utilizzo.')</script>";
	        $uploadOk = 0;
	    }*/
	    // Check file size
	    if ($_FILES[$type]["size"] > 512000) {
	        echo "<script type='text/javascript'>alert('Il file è troppo pesante.')</script>";
	        $uploadOk = 0;
	    }
	    // Allow certain file formats
	    if ($imageFileType != "" && $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
	        echo "<script type='text/javascript'>alert('Sono supportati solamente i seguenti formati: JPG, JPEG, PNG e GIF')</script>";
	        $uploadOk = 0;
	    }
	    // Check if $uploadOk is set to 0 by an error
	    if ($uploadOk == 0) {
	        echo "<script type='text/javascript'>alert('Impossibile caricare file al momento')</script>";
	        // if everything is ok, try to upload file
	    } else {
				$new_name = $dir . "/" . $user->slug . "-" . $type . "." . $imageFileType;
	        if (!move_uploaded_file($_FILES[$type]["tmp_name"], $new_name)) {
	            echo "<script type='text/javascript'>alert('Errore durante il caricamento dell'immagine, si prega di riprovare.')</script>";
	        } else {
	            return $new_name;
	        }
	    }
	}
}
