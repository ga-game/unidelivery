<?php
namespace Controllers;

class ApiFormValidationController extends JsonController {
  public function postAssertEmailUnregistered() {
    $email = request_value('email', '');

    $valid = false;
    $msg = 'Email mal formattata';
    if(filter_var($email, FILTER_VALIDATE_EMAIL)){
      $found = db_query('SELECT email FROM User WHERE email=? LIMIT 1', [$email]);
      $valid = count($found) == 0;
      if($valid) {
        $msg = '';
      } else {
        $msg = 'Email già registrata';
      }
    }
    echo json_encode(['valid' => $valid, 'message' => $msg]);
  }

  public function postAssertSupplierUnregistered() {
    $name = request_value('name', '');

    $valid = false;
    $msg = 'Nome troppo corto';
    if(strlen($name) >= 3) {
      $found = db_query('SELECT name FROM User WHERE role="supplier" AND name=? LIMIT 1', [$name]);
      $valid = count($found) == 0;
      if($valid) {
        $msg = '';
      } else {
        $msg = 'Nome già registrato';
      }
    }
    echo json_encode(['valid' => $valid, 'message' => $msg]);
  }
}
