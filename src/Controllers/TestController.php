<?php

namespace Controllers;

class TestController {
  protected const TRACK_PREVIOUS_URL = false;

  public function get() {
    echo "GET<br/>";
    var_dump($_GET);
    echo "<br/>";
    if(isset($_REQUEST['crypt'])) {
      echo password_hash($_REQUEST['crypt'], PASSWORD_DEFAULT);
    }
    die();
  }

  public function post() {
    echo "POST<br/>";
    var_dump($_POST);
    die();
  }

  public function getMail() {
    $mail = new \Mail('subject', ['body bold', 'body <b>bold</b>'], 2);
    $mail->sendTo('alessandro.oliva4@studio.unibo.it');
  }
}
