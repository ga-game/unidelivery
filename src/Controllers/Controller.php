<?php
namespace Controllers;

class Controller {
  protected const REQUIRE_AUTH = FALSE;
  protected const TRACK_PREVIOUS_URL = TRUE;

  public function __construct() {
    if(static::REQUIRE_AUTH){
      \Auth::require();
    }
    if(static::TRACK_PREVIOUS_URL){
      save_back_url();
    }
  }
};
