<?php
namespace Controllers;
use \Models\User;
use \Models\Customer;
use \Models\Supplier;
use \Auth;
use \Mail;

class AccessController extends Controller {
  protected const TRACK_PREVIOUS_URL = false;
  protected const MAX_ATTEMPS = 5;

  public function __construct() {
    parent::__construct();
    if(Auth::get() !== NULL && $_SERVER['REQUEST_URI'] !== '/logout'){
      redirect_back();
    }
  }

  public function getLogin() {
    load_view('login');
  }

  public function getSignupCustomer() {
    load_view('signup');
  }

  public function getSignupSupplier() {
    load_view('signup-supplier');
  }

  public function postLogin() {
    $email = $_POST['email'];
    $password = $_POST['password']; // nota: è già criptata con bcrypt!

    $user = User::findByEmail($email);
    if(is_null($user)){
      redirect('/login?error=pwd');
    } else {
      $user->addLoginAttemp();
      if($user->getLoginAttemps() > self::MAX_ATTEMPS) {
        redirect('/login?error=attemps');
      } else if(!hash_equals($user->password, $password)){
         // usare if(!password_verify($password, $user->password)){} SE non è già criptata
        redirect('/login?error=pwd');
      } else if(!empty($user->confirmationCode)) {
        redirect('/confermare-account');
      } else if($user->banned){
        redirect('/login?error=banned');
      } else {
        Auth::remember($user, isset($_POST['remember-me']));
        $user->resetLoginAttemps();
        $obs = $this->getLoginHandler();
        if(is_null($obs)){
          redirect_back();
        } else {
          self::setLoginHandler(null);
          $obs->onLogin($user);
        }
      }
    }
  }

  public function postLogout() {
    Auth::logout();
    redirect_back();
  }

  public function postSignupCustomer() {
    $confCode = random_string(10);
    $user = Customer::create([
      'email' => $_POST['email'],
      'password' => $_POST['password'], // se non già criptata, usare password_hash($_POST['password'], PASSWORD_DEFAULT),
      'name' => $_POST['name'],
      'shortName' => strtok($_POST['name'], ' '),
      'confirmationCode' => $confCode
    ]);
    $this->goToConfirmation($confCode);
  }

  public function postSignupSupplier() {
    $confCode = random_string(10);
    $user = Supplier::create([
      'email' => $_POST['email'],
      'password' => $_POST['password'], // se non già criptata, usare password_hash($_POST['password'], PASSWORD_DEFAULT),
      'name' => $_POST['name'],
      'shortName' => $_POST['name'],
      'confirmationCode' => $confCode,
      'slug' => url_slug($_POST['name']),
      'logopath' => '/res/default-icon.jpg',
      'imgpath' => '/res/default-bg.png',
    ]);
    $this->goToConfirmation($confCode);
  }

  private function goToConfirmation($code) {
    $confirmationLink = 'http://'.$_SERVER['HTTP_HOST'].'/conferma?codice='.$code;
    $mail = new Mail('Conferma registrazione', [
      "Benvenuto in UniDelivery! \r\nConferma il tuo account navigando sul link seguente:\r\n$confirmationLink",
      "Benvenuto in UniDelivery! </br>Conferma il tuo account navigando su <strong><a href=\"$confirmationLink\">questo link</a></strong>"
    ]);
    $mail->sendTo($_POST['email']);
    redirect('/confermare-account');
  }

  public function getConfirmAccount() {
    load_view('please-confirm-account');
  }

  public function getDoConfirm() {
    $confCode = $_GET['codice'];
    $usr = User::findByConfirmationCode($confCode);
    if(is_null($usr)){
      abort(404, 'Codice account non trovato');
    }
    $usr->confirmAccount();
    load_view('account-confirmed');
  }

  public static function setLoginHandler($handler){
    if(is_null($handler)){
      unset($_SESSION['login-callback-handler']);
    } else {
      $_SESSION['login-callback-handler'] = get_class($handler);
    }
  }

  private function getLoginHandler() {
    $cls = isset($_SESSION['login-callback-handler']) ? $_SESSION['login-callback-handler'] : null;
    self::setLoginHandler(null);
    return is_null($cls) ? null : new $cls;
  }
}
