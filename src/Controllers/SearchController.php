<?php
namespace Controllers;

class SearchController extends Controller {
  public function get(){
    $searchQuery = request_value('cerca', '');
    load_view('restaurants', [
      'query' => $searchQuery,
      'restaurants' => \Models\Supplier::search($searchQuery),
    ]);
  }

  public function getRestaurant($slug){
	  
	  if (isset($_SESSION['order-request-' . $slug])) {
		$data = $_SESSION['order-request-' . $slug];
		$pids = array_keys($data['product']);
		$prods = \Models\Product::findByIds($pids);
	  } else {
		$data = [];
		$prods = [];
	  }
	

    $price = 0;
    $chart = [];
    foreach($prods as $prod){
      $price += $prod->price * $data['product'][$prod->id];
      $chart[] = ['product' => $prod, 'quantity' => $data['product'][$prod->id]];
    }
	  
    $searchQuery = request_value('cerca', '');
    $rest = \Models\Supplier::findBySlug($slug) or abort(404);
    load_view('restaurant-menu', [
      'restaurant' => $rest,
      'query' => $searchQuery,
	  'chart' => $chart,
	  'totalPrice' => $price
    ]);
  }
}
