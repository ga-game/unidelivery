<?php
namespace Controllers;

class AdminController extends Controller {
  
  public function getAdminControlPanel(){
	  
	$user = \Auth::require();
	if ($user->isAdmin()) {
		load_view('admin-control-panel', []);
	} else {
		redirect('/');
	}
  }
  
  public function postAdminControlPanel(){ 
	if (isset($_REQUEST["add-tag"])) {
		$addedSupplierTag = $_REQUEST["add-tag"];
		$q = db_exec('SELECT * FROM Suppliertag WHERE name=?', [$addedSupplierTag]);
		if ($q->rowCount() === 0) {
			db_exec('INSERT INTO Suppliertag VALUES (?)', [$addedSupplierTag]);
		}
    }
	
	if (isset($_REQUEST["id-ban"])) {
		$userId = $_REQUEST["id-ban"];
		$q = db_exec('UPDATE User SET banned=true, rememberToken=null WHERE id=?', [$userId]);
    }
	
	if (isset($_REQUEST["id-unban"])) {
		$userId = $_REQUEST["id-unban"];
		$q = db_exec('UPDATE User SET banned=false WHERE id=?', [$userId]);
    }
	
	if (isset($_REQUEST["remove-section"])) {
		$removedSupplierTag = $_REQUEST["remove-section"];
		db_exec('DELETE FROM Suppliertag WHERE name=?', [$removedSupplierTag]);
    }
		
	load_view('admin-control-panel', []);
  }
}
