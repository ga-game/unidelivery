<?php
namespace Controllers;

class HtmlValidatorController extends Controller {
  protected const TRACK_PREVIOUS_URL = false;

  public function post($validator) {
    $html = $_REQUEST['html'];

    set_time_limit(20);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT'] ?? '');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch,CURLOPT_TIMEOUT,10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

    switch($validator){
      case 'achecker':
      $url = 'https://achecker.ca/checker/index.php';
      $postData = [
  	    'uri' => '',
  	    'MAX_FILE_SIZE' => 52428800,
  	    'uploadfile' => '(binary)',
  	    'pastehtml' => $html,
  	    'validate_paste' => 'Check It',
  	    'radio_gid[]' => 8,
  	    'checkbox_gid[]' => 8,
  	    'rpt_format' => 1
      ];
      curl_setopt($ch, CURLOPT_POST, 1);
      break;

      case 'w3':
      $url = 'https://validator.w3.org/nu/';
      $postData = [
        'content' => $html,
        'showsource' => 'yes'
      ];
      //curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: multipart/form-data; charset=utf-8']);
      //curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: text/html; charset=utf-8']);
      break;
      
      default:
      abort(404);
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $response = curl_exec( $ch );
    curl_close($ch);

    if($validator === 'w3'){
      $response = str_replace('<head>', '<head><base href="' . $url . '">', $response);
    }
    echo $response;
  }
}
