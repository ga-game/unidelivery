<?php
namespace Controllers;

use \Models\MenuSection;
use \Models\Supplier;
use \Models\Product;
use \Auth;

class SupplierPanelController extends Controller {
  public function getEditMenu() {
    $user = Auth::require();

	if ($user->isSupplier()) {
		load_view('edit-list', [
		  'menu' => $user->getMenu()
		]);
	} else {
		redirect('/');
	}
  }

  public function postEditMenu() {
    $user = Auth::require();
    $user->deleteMenuSections();
	$user->deleteMenuProducts();
    $sections = [];
    $pos = 0;
    foreach(request_value('section', []) as $sectionName) {
      $sections[$sectionName['name']] = MenuSection::create([
        'name' => $sectionName['name'],
        'supplier' => $user->id,
        'pos' => $pos
      ]);
      $pos++;
    }

    foreach(request_value('product', []) as $product) {
      $section = $sections[$product['section']];
      $product = Product::create([
        'name' => $product['name'],
        'price' => $product['price'],
        'section' => $section->id
      ]);
    }
    redirect('/ristoranti?cerca=');
  }
}
