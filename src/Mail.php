<?php

require_once (__DIR__."\lib\PHPMailer\src\PHPMailer.php");
require_once (__DIR__."\lib\PHPMailer\src\SMTP.php");
require_once (__DIR__."\lib\PHPMailer\src\Exception.php");
use \PHPMailer\PHPMailer\PHPMailer;

class Mail {
  private $mail;

  public function __construct($subject, $body, $debug = 0) {
    $this->mail = new PHPMailer();
    $this->mail->IsSMTP(); // telling the class to use SMTP
    $this->mail->SMTPDebug  = $debug; // enables SMTP debug information (for testing)
    // 1 = errors and messages
    // 2 = messages only

    $this->mail->SMTPAuth   = true;                  // enable SMTP authentication
    $this->mail->SMTPSecure = "tls";
    $this->mail->Host       = "smtp.gmail.com";      // SMTP server
    $this->mail->Port       = 587;                   // SMTP port
    $this->mail->Username   = "info.unidelivery@gmail.com";  // username
    $this->mail->Password   = "unidelivery18+";            // password
    $this->mail->CharSet = 'UTF-8';
    $this->mail->Encoding = 'base64';
    $this->mail->SetFrom('info.unidelivery@gmail.com', 'UniDelivery');
    $this->mail->Subject = $subject;
    if(is_array($body)){
      $this->mail->MsgHTML($body[1]);
      $this->mail->AltBody = $body[0];
    } else {
      $this->mail->isHTML(false);
      $this->mail->Body = $body;
    }
  }

  public function sendTo($to) {
    $this->mail->ClearAllRecipients();
    $this->mail->AddAddress($to);
    return $this->mail->Send();
  }
}
