<?php
use \Models\User;

class Auth {
  private static $usr = null;

  public static function remember($user, $longTime=false) {
    $_SESSION['user-id'] = $user->id;
    self::$usr = $user;
    if($longTime){
      $rememberToken = random_string(10);
      $user->setRememberToken($rememberToken);
      // 30 giorni
      setcookie('remember', $rememberToken, time() + 60*60*24*30);
    }
  }

  public static function get() {
    if(is_null(self::$usr)){
      if(isset($_SESSION['user-id'])){
        self::$usr = User::findById($_SESSION['user-id']);
      }else if(isset($_COOKIE['remember'])){
        self::$usr = User::findByRememberToken($_COOKIE['remember']);
      }
    }
    return self::$usr;
  }

  public static function require() {
    $us = self::get();
    if(is_null($us)){
      redirect('/login');
      exit();
    } else if ($us->banned) {
	    $us->setRememberToken(null);
		unset($_SESSION['user-id']);
		session_destroy();
		redirect('/');
	}
    return $us;
  }

  public static function logout() {
    $us = self::require();
    $us->setRememberToken(null);
    unset($_SESSION['user-id']);
    session_destroy();
	redirect('/');
  }
}
