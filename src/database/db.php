<?php
require_once('../config.php');
require_once('../utils.php');


function __init_pdo($pdo) {
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}

function __init_db($pdo) {
  $sql = file_get_contents(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'init_db.sql');
  $queries = explode("\n\n", $sql);
  foreach($queries as $query) {
    try{
      $pdo->exec($query);
    }catch(PDOException $e){
      echo $query . "<br/>\n";
      throw $e;
    }
  }
}

function db_init() {
  $con = db_connect();
  __init_db($con);
}

function db_connect() {
  static $initialized = false;
  static $con = null;
  if(!$initialized){
    try {
      $con = @new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
      __init_pdo($con);
    } catch(PDOException $e)  {
      $con = new PDO('mysql:host=' . DB_HOST, DB_USER, DB_PASS);
      __init_pdo($con);
      __init_db($con);
    }
    $initialized = true;
  }
  return $con;
}

function db_raw_query($query) {
  $con = db_connect();
  return $con->query($query)->fetchAll();
}
function db_raw_exec($query) {
  $con = db_connect();
  return $con->exec($query);
}

function db_query($query, $parameters = []) {
  if(count($parameters)<=0){
    return db_raw_query($query);
  }
  $con = db_connect();
  $stmt = $con->prepare($query);
  $stmt->execute($parameters);
  return $stmt->fetchAll();
}

function db_exec($query, $parameters = []) {
  if(count($parameters)<=0){
    return db_raw_exec($query);
  }
  $con = db_connect();
  $stmt = $con->prepare($query);
  $stmt->execute($parameters);
  return $stmt;
}

function db_last_insert_id() {
  $con = db_connect();
  return $con->lastInsertId();
}
