DROP DATABASE IF EXISTS unidelivery;

CREATE DATABASE unidelivery;

USE unidelivery;

CREATE TABLE User(
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(255) NOT NULL UNIQUE KEY,
  password VARCHAR(255) NOT NULL,
  name VARCHAR(100) NOT NULL,
  shortName VARCHAR(100) NOT NULL,
	phone VARCHAR(10) NULL DEFAULT NULL UNIQUE KEY,
	address VARCHAR(100) NULL DEFAULT NULL UNIQUE KEY,
  role VARCHAR(20) NOT NULL,
  confirmationCode VARCHAR(10) NULL DEFAULT NULL UNIQUE KEY,
  rememberToken VARCHAR(10) NULL DEFAULT NULL UNIQUE KEY,
  slug VARCHAR(100) NULL DEFAULT NULL UNIQUE KEY,
  logoPath VARCHAR(200) NULL DEFAULT NULL,
  imgPath VARCHAR(200) NULL DEFAULT NULL,
  banned BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE LoginAttemps (
  user INT UNSIGNED NOT NULL,
  date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (user) REFERENCES User(id) ON DELETE CASCADE,
  PRIMARY KEY (user, date)
);

CREATE TABLE SupplierTag (
  name VARCHAR(50) NOT NULL PRIMARY KEY
);

CREATE TABLE Supplier_Tagged (
  supplier INT UNSIGNED NOT NULL,
  tag VARCHAR(50) NOT NULL,
  FOREIGN KEY (supplier) REFERENCES User(id) ON DELETE CASCADE,
  FOREIGN KEY (tag) REFERENCES SupplierTag(name) ON DELETE CASCADE,
  PRIMARY KEY (supplier, tag)
);

CREATE TABLE MenuSection (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  supplier INT UNSIGNED NOT NULL,
  pos INT NOT NULL DEFAULT 0,
  name VARCHAR(50) NOT NULL,
  deleted BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (supplier) REFERENCES User(id)
);

CREATE TABLE Product (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  section INT UNSIGNED NOT NULL,
  posInSection INT NOT NULL,
  name VARCHAR(50) NOT NULL,
  price DECIMAL(6,2),
  deleted BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (section) REFERENCES MenuSection(id)
);

CREATE TABLE Location (
  name VARCHAR(100) NOT NULL PRIMARY KEY
);

CREATE TABLE `Order` (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  customer INT UNSIGNED NOT NULL,
  dateDelivery DATETIME DEFAULT NULL,
  datePayment DATETIME DEFAULT NULL,
  location VARCHAR(100) NOT NULL,
  status VARCHAR(100) NOT NULL DEFAULT 'queued',
  comment VARCHAR(200) NOT NULL DEFAULT '',
  FOREIGN KEY (location) REFERENCES Location(name),
  FOREIGN KEY (customer) REFERENCES User(id)
);

CREATE TABLE Order_Product (
  `order` INT UNSIGNED NOT NULL,
  product INT UNSIGNED NOT NULL,
  quantity INT UNSIGNED NOT NULL,
  FOREIGN KEY (`order`) REFERENCES `Order`(id),
  FOREIGN KEY (product) REFERENCES Product(id),
  PRIMARY KEY (`order`, product)
);

CREATE TABLE Notification (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  destination INT UNSIGNED NOT NULL,
  `order` INT UNSIGNED NOT NULL,
  dateCreated DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  dateRead DATETIME DEFAULT NULL,
  type VARCHAR(100) NOT NULL,
  FOREIGN KEY (`order`) REFERENCES `Order`(id),
  FOREIGN KEY (destination) REFERENCES User(id)
);

INSERT INTO User(id, name, shortName, email, password, role) VALUES
(2, 'Gianni Verdi', 'Gianni', 'gianni@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', 'customer');

INSERT INTO User(id, name, shortName, slug, email, password, role) VALUES
(1, 'admin', 'Amministratore', 'Admin', 'admin@unidelivery.it', 'C7AD44CBAD762A5DA0A452F9E854FDC1E0E7A52A38015F23F3EAB1D80B931DD472634DFAC71CD34EBC35D16AB7FB8A90C81F975113D6C7538DC69DD8DE9077EC', 'admin');

INSERT INTO User(id, name, shortName, slug, email, password, phone, address, role, logoPath, imgPath) VALUES
(3, 'Abdul Aladino', 'Abdul', 'abdul-aladino', 'abdul@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547113344', 'Via Pio IX, 11', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png'),
(4, 'Pizzeria Da Luigi', 'Luigi', 'pizzeria-da-luigi', 'luigi@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547223355', 'Via Giro Tondo, 14', 'supplier', '/res/supplier-icons/pizzeria-da-luigi-icon.png', '/res/supplier-covers/pizzeria-da-luigi-cover.jpg'),
(5, 'Ronald McBurger', 'Ronald', 'ronald-mcburger', 'ronald@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547116634', 'Viale Largo, 3', 'supplier', '/res/supplier-icons/ronald-mcburger-icon.jpg', '/res/supplier-covers/ronald-mcburger-cover.jpg'),
(6, 'Sakura Sushi', 'Sakura', 'sakura-sushi', 'sakura@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547127788', 'Via Stretta, 1', 'supplier', '/res/supplier-icons/sakura-sushi-icon.jpg', '/res/supplier-covers/sakura-sushi-cover.jpg'),
(7, 'Ping Pong Ristorante Cinese', 'Ping', 'ping-pong-ristorante-cinese', 'ping@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547128899', 'Via Lunga, 2A', 'supplier', '/res/supplier-icons/ping-pong-ristorante-cinese-icon.jpg', '/res/supplier-covers/ping-pong-ristorante-cinese-cover.jpg'),
(8, 'Rosticceria Da Giuseppe', 'Giuseppe', 'rosticceria-da-giuseppe', 'giuseppe@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547223344', 'Viale Corto, 4', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png'),
(9, 'Antichi Sapori', 'Domenico', 'antichi-sapori', 'domenico@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547112255', 'Via Col Vento, 66', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png'),
(10, 'Ahmed Jaffar', 'Ahmed', 'ahmed-jaffar', 'ahmed@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547557700', 'Via Vai, 13', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png'),
(11, 'Piadineria Genoveffa', 'Genoveffa', 'piadineria-genoveffa', 'genoveffa@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547110044', 'Piazza Grande', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png'),
(12, 'Muhammad Kebab', 'Muhammad', 'muhammad-kebab', 'muhammad@live.it', '3627909A29C31381A071EC27F7C9CA97726182AED29A7DDD2E54353322CFB30ABB9E3A6DF2AC2C20FE23436311D678564D0C8D305930575F60E2D3D048184D79', '0547132244', 'Vicolo Nuovo, 43A', 'supplier', '/res/default-icon.jpg', '/res/default-cover.png');

INSERT INTO SupplierTag(name) VALUES
('Italiano'),
('Pizzeria'),
('Piadineria'),
('Fastfood'),
('Sushi'),
('Kebab'),
('Rosticceria'),
('Cinese');

INSERT INTO Location(name) VALUES
('Segreteria'),
('Aula 2.1'),('Aula 2.2'),('Aula 2.3'),('Aula 2.4'),('Aula 2.5'),('Aula 2.6'),('Aula 2.7'),('Aula 2.8'),('Aula 2.9'),
('Aula 2.10'),('Aula 2.11'),('Aula 2.12'),('Aula 2.13'),('Aula Magna 3.4'),('Aula 3.7'),('Aula 3.10'),('Aula 3.11'),('Aula 4.1');

INSERT INTO Supplier_Tagged(supplier, tag) VALUES
(3, 'Kebab'),
(3, 'Pizzeria'),
(4, 'Pizzeria'),
(5, 'Fastfood'),
(6, 'Sushi'),
(7, 'Cinese'),
(8, 'Rosticceria'),
(9, 'Italiano'),
(10, 'Kebab'),
(11, 'Piadineria'),
(12, 'Kebab');

INSERT INTO MenuSection(id, supplier, pos, name) VALUES
(1, 3, 0, 'Cibo'), (2, 3, 1, 'Bibite');

INSERT INTO Product(id, section, name, price) VALUES
(1, 1, 'Kebab', 3.50), (2, 1, 'Roll Kebab', 4.00), (3, 1, 'King Kebab', 6.00),
(4, 2, 'Coca', 1.50), (5, 2, 'Fanta', 1.50), (6, 2, 'Acqua 50cl', 1.00);


INSERT INTO `Order`(id, customer, dateDelivery, datePayment, location, status, comment) VALUES
(1, 2, NOW(), NOW(), 'Aula 2.2', 'sent', ''), (2, 2, NOW(), NOW(), 'Segreteria', 'sent', '');

INSERT INTO Notification(id, destination, `order`, dateCreated, dateRead, type) VALUES
(1, 2, 1, NOW(), NULL, 'order-sent'), (2, 2, 2, NOW(), NULL, 'order-sent');

INSERT INTO Order_Product(`order`, product, quantity) VALUES
(1, 1, 3), (1, 2, 1), (2, 6, 5);
