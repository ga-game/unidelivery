# tweb-elaborato

## WOW
* Login sicuro come da guida segnalata nel materiale del corso
* Password criptate
* Utilizzo di AJAX per la visualizzazione di notifiche senza dover ricaricare la pagina
* Utilizzo AJAX per validazione form di registrazione (check che email non esista già)
* Utilizzo javascript/jquery per validare form durante l'immissione dei dati e per restituire un feedback in tempo reale
* Script per validare html delle pagine tramite W3C e ACheker automaticamente. Tale script è necessario perchè l'html aggiunto via javascript NON viene visualizzato facendo tasto destro -> visualizza sorgente pagina. Lo script invece include tutto l'html esistente al momento della pressione del tasto.
* Registrazione con conferma dell'account via email
* Notifiche anche per email
* Pretty urls



## Istruzioni setup

1. Clonare dentro xampp/htdocs
2. Disattivare antivirus e firewall. Abbiamo notato che alcuni di questi bloccano il traffico smtp in uscita, ovvero l'invio delle email di conferma e notifiche.
3. Impostare la DocumentRoot di Apache a src/www (es DocumentRoot "C:/xampp/htdocs/tweb-elaborato/src/www")

  AllowOverride All  
  esempio:  



```
  DocumentRoot "C:/xampp/htdocs/tweb-elaborato/src/www"
  <Directory "C:/xampp/htdocs/tweb-elaborato/src/www">
      #
      # Possible values for the Options directive are "None", "All",
      # or any combination of:
      #   Indexes Includes FollowSymLinks SymLinksifOwnerMatch ExecCGI MultiViews
      #
      # Note that "MultiViews" must be named *explicitly* --- "Options All"
      # doesn't give it to you.
      #
      # The Options directive is both complicated and important.  Please see
      # http://httpd.apache.org/docs/2.4/mod/core.html#options
      # for more information.
      #
      Options Indexes FollowSymLinks Includes ExecCGI

      AllowOverride All

      Require all granted
  </Directory>
```
